#pragma once

#include <fstream>
#include <QtWidgets/QMainWindow>
#include <QListWidget>
#include <QRadioButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QCloseEvent>

#include "ui_AdminInterface.h"

#include "../Logger/Logger.h"
#include "../QuestionnaireFramework/ChoiceQuestion.h"
#include "../QuestionnaireFramework/QuestionBank.h"

#include "AdminConstants.h"

class AdminInterface : public QMainWindow {
private:
	Q_OBJECT;
	Ui::AdminInterfaceClass ui;

	std::ofstream loggerStream;
	std::shared_ptr<Logger> logger;

	QuestionBank questionBank;
	std::atomic_bool chaptersLoaded = false;

	uint16_t currentQuestionIndex;
	uint16_t currentChapterIndex;

	std::shared_ptr<Question> question = nullptr;
	bool comboBoxInput = false;
	bool isEditing = false;

	std::string fileName;

	std::array<QRadioButton*, ChoiceQuestion::ANSWERS_NUMBER> singleChoiceButtons;
	std::array<QCheckBox*, ChoiceQuestion::ANSWERS_NUMBER> multipleChoiceButtons;
	std::array<QLineEdit*, ChoiceQuestion::ANSWERS_NUMBER> lineEditsRadio;
	std::array<QLineEdit*, ChoiceQuestion::ANSWERS_NUMBER> lineEditsCheck;

public:
	AdminInterface(const std::string& questionsFile,
		QWidget* parent = Q_NULLPTR);

private:
	void init();
	void initWindow();
	void initMainFrame();
	void initChapterList();
	void initQuestionFrame();
	void initQuestionComboBox();
	void initQuestionTableWidget();
	void updateChapterLayout();
	void addTableItem(const uint16_t row, const uint8_t column, const uint16_t questionIndex);
	void uncheckSingleChoice(const uint8_t index);
	void updateQuestionInfo();
	void updateQuestionLayout();
	bool saveQuestion(bool addLastQuestion = false);
	bool saveSingleChoiceQuestion(const std::string& questionText, const bool chapterQuestion, const bool addLastQuestion);
	bool saveMultipleChoiceQuestion(const std::string& questionText, const bool chapterQuestion, const bool addLastQuestion);
	bool saveInputQuestion(const std::string& questionText, const bool chapterQuestion, const bool addLastQuestion);
	void overrideQuestion(const bool chapterQuestion, const bool addLastQuestion, const std::shared_ptr<Question>& newQuestion);
	void showCompletionError(const QString& error);
	bool checkDuplicatedAnswers(const std::array<QLineEdit*, ChoiceQuestion::ANSWERS_NUMBER>& answers) const;
	void setTableItemSelected(uint16_t index, bool selected);
	void addItemToList(const uint16_t index);
	void previousNextQuestionClicked(bool previous);
	void updateTableWidget(bool add);
	void buttonClicked(const QString& buttonName);
	inline const uint16_t getChapterQuestionsNumber() const;
	inline const QString getChapterItemSuffix(const uint16_t index) const;
	bool eventFilter(QObject* obj, QEvent* event);
	void closeEvent(QCloseEvent* event);

private slots:
	void showDeleteOption(const QPoint& point);
	void eraseChapter();
	void renameChapter();
	void listItemClicked(QListWidgetItem* item);
	void listItemChanged(QListWidgetItem* item);
	void textEntered(const QString& text);
	void addChapterClicked();
	void homeButtonClicked();
	void questionComboBoxIndexChanged(int index);
	void tableItemClicked(int row, int column);
	void previousQuestionClicked();
	void nextQuestionClicked();
	void addRemoveQuestionClicked();
};
