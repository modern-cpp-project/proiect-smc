﻿#include <QString>

#include "AdminConstants.h"

const std::string K::PROGRAM_NAME = "AdminInterface";
const QString K::CANNOT_OPEN_FILE = QString::fromWCharArray(L"Nu se poate deschide fișierul cu întrebări. Vă rugăm încercați din nou.");
const QString K::ERASE_CHAPTER = QString::fromStdWString(L"Șterge capitolul");
const QString K::RENAME_CHAPTER = QString::fromStdWString(L"Redenumește capitolul");
const QString K::QUESTION = QString::fromStdWString(L" întrebare");
const QString K::QUESTIONS = QString::fromStdWString(L" întrebări");
const QString K::ADD_QUESTION = QString::fromStdWString(L"Adaugă întrebarea");
const QString K::REMOVE_QUESTION = QString::fromStdWString(L"Șterge întrebarea");
const QString K::SINGLE_CHOICE_QUESTION_TITLE = QString::fromStdWString(L"Întrebare cu răspuns unic");
const QString K::MULTIPLE_CHOICE_QUESTION_TITLE = QString::fromStdWString(L"Întrebare cu răspuns multiplu");
const QString K::INPUT_QUESTION_TITLE = QString::fromStdWString(L"Întrebare cu răspuns text");
const QString K::RADIO_BUTTON = "radioButton";
const QString K::CHECK_BOX = "checkBox";
const QString K::LINE_EDIT_RADIO = "lineEditRadio";
const QString K::LINE_EDIT_CHECK = "lineEditCheck";
const QString K::QUESTION_TEXT_NOT_COMPLETED = QString::fromStdWString(L"Vă rugăm să completați textul întrebării");
const QString K::ANSWERS_NOT_COMPLETED = QString::fromStdWString(L"Vă rugăm să completați toate răspunsurile");
const QString K::CORRECT_ANSWER_NOT_COMPLETED = QString::fromStdWString(L"Vă rugăm să completați răspunsul corect");
const QString K::CORRECT_ANSWER_NOT_SELECTED = QString::fromStdWString(L"Vă rugăm să selectați răspunsul corect");
const QString K::CORRECT_ANSWERS_NOT_SELECTED = QString::fromStdWString(L"Vă rugăm să selectați răspunsul / răspunsurile corect(e)");
const QString K::DUPLICATED_ANSWERS = QString::fromStdWString(L"Vă rugăm să eliminați răspunsurile duplicate");
const QColor K::WHITE = QColor(255, 255, 255);

const std::string K::LOG::FILE = "../output/AdminInterfaceLog.txt";
const std::string K::LOG::PRESSED = " PRESSED";
const std::string K::LOG::CREATED_SINGLE_CHOICE_QUESTION = "Created single choice question";
const std::string K::LOG::CREATED_MULTIPLE_CHOICE_QUESTION = "Created multiple choice question";
const std::string K::LOG::CREATED_INPUT_QUESTION = "Created input question";
const QString K::LOG::PREVIOUS_QUESTION = "previousQuestion";
const QString K::LOG::NEXT_QUESTION = "nextQuestion";
const QString K::LOG::ADD_CHAPTER = "Add chapter ";
const QString K::LOG::HOME_BUTTON = "Home button ";
const QString K::LOG::ADD_QUESTION = "Add question ";
const QString K::LOG::REMOVE_QUESTION = "Remove question ";
const QString K::LOG::ERASE_CHAPTER = "Erase chapter ";
const QString K::LOG::TABLE_ITEM = "Table item ";
const QString K::LOG::LIST_ITEM = "List item ";
const QString K::LOG::COMBO_BOX_ITEM = "Combo box item ";
