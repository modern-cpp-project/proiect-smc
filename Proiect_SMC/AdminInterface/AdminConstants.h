#pragma once
#include <string>
#include <QColor>

class K {
	friend class AdminInterface;

	static const uint8_t MINIMUM_CHAPTER_NAME_SIZE = 2;
	static const uint8_t MAXIMUM_COLUMNS_QUESTION_TABLE = 5;
	static const uint8_t SINGLE_CHOICE_INDEX = 0;
	static const uint8_t MULTIPLE_CHOICE_INDEX = 1;
	static const uint8_t INPUT_INDEX = 2;
	static const std::string PROGRAM_NAME;
	static const QString CANNOT_OPEN_FILE;
	static const QString ERASE_CHAPTER;
	static const QString RENAME_CHAPTER;
	static const QString QUESTION;
	static const QString QUESTIONS;
	static const QString ADD_QUESTION;
	static const QString REMOVE_QUESTION;
	static const QString SINGLE_CHOICE_QUESTION_TITLE;
	static const QString MULTIPLE_CHOICE_QUESTION_TITLE;
	static const QString INPUT_QUESTION_TITLE;
	static const QString RADIO_BUTTON;
	static const QString CHECK_BOX;
	static const QString LINE_EDIT_RADIO;
	static const QString LINE_EDIT_CHECK;
	static const QString QUESTION_TEXT_NOT_COMPLETED;
	static const QString ANSWERS_NOT_COMPLETED;
	static const QString CORRECT_ANSWER_NOT_COMPLETED;
	static const QString CORRECT_ANSWER_NOT_SELECTED;
	static const QString CORRECT_ANSWERS_NOT_SELECTED;
	static const QString DUPLICATED_ANSWERS;
	static const quint64 ENCRYPTION_KEY = Q_UINT64_C(0x0c5bc8a5eba9f852);
	static const QColor WHITE;

	class LOG {
	public:
		static const std::string FILE;
		static const std::string PRESSED;
		static const std::string CREATED_SINGLE_CHOICE_QUESTION;
		static const std::string CREATED_MULTIPLE_CHOICE_QUESTION;
		static const std::string CREATED_INPUT_QUESTION;
		static const QString PREVIOUS_QUESTION;
		static const QString NEXT_QUESTION;
		static const QString ADD_CHAPTER;
		static const QString HOME_BUTTON;
		static const QString ADD_QUESTION;
		static const QString REMOVE_QUESTION;
		static const QString ERASE_CHAPTER;
		static const QString TABLE_ITEM;
		static const QString LIST_ITEM;
		static const QString COMBO_BOX_ITEM;
	};
};