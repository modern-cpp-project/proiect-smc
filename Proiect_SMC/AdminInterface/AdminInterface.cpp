#include <thread>
#include <set>
#include <sstream>
#include <QKeyEvent>
#include <QMenu>
#include <QDesktopWidget>

#include "AdminInterface.h"

#include "../QuestionnaireFramework/SingleChoiceQuestion.h"
#include "../QuestionnaireFramework/MultipleChoiceQuestion.h"
#include "../QuestionnaireFramework/InputQuestion.h"
#include "../SimpleCrypt/SimpleCrypt.h"

AdminInterface::AdminInterface(const std::string& questionsFile, QWidget* parent)
	: loggerStream(std::ofstream(K::LOG::FILE)),
	logger(std::make_shared<Logger>(loggerStream)),
	fileName(questionsFile),
	QMainWindow(parent) {
	init();
	std::thread([this, questionsFile]() {
		try {
			std::ifstream ist(fileName);
			if (!ist.is_open()) {
				throw K::CANNOT_OPEN_FILE;
			}
			std::stringstream ss;
			std::string line;
			SimpleCrypt crypto(K::ENCRYPTION_KEY);
			while (std::getline(ist, line)) {
				ss << crypto.decryptToString(QString::fromStdString(line)).toStdString() << std::endl;
			}
			ist.close();
			ss.seekg(std::ios_base::beg);
			questionBank = QuestionBank(ss, logger);
			chaptersLoaded = true;
			QMetaObject::invokeMethod(
				this,
				[=]() {
					initChapterList();
					ui.addChapter->setEnabled(ui.chapterLineEdit->text().length() > K::MINIMUM_CHAPTER_NAME_SIZE);
				});
		}
		catch (const QString& errorMessage) {
			QMetaObject::invokeMethod(
				this,
				[=]() {
					ui.errorLabel->setText(errorMessage);
					ui.chapterWidget->setVisible(false);
					ui.errorLabel->setVisible(true);
				});
			logger->log(errorMessage.toStdString(), Logger::Level::ERROR);
		}
		}).detach();
}

void AdminInterface::init() {
	ui.setupUi(this);
	initWindow();
	initMainFrame();
	initQuestionFrame();
}

void AdminInterface::initWindow() {
	QSize size = QDesktopWidget().availableGeometry(this).size();
	setMinimumSize(size / 1.5);
	resize(size * 0.67);
}

void AdminInterface::initMainFrame() {
	ui.errorLabel->setVisible(false);
	ui.chapterLineEdit->installEventFilter(this);
	ui.chapterList->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui.chapterLineEdit, SIGNAL(textEdited(QString)), this, SLOT(textEntered(QString)));
	connect(ui.addChapter, SIGNAL(clicked()), this, SLOT(addChapterClicked()));
	connect(ui.chapterList, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showDeleteOption(QPoint)));
}

void AdminInterface::initChapterList() {
	for (uint16_t index = 0; index < questionBank.getChaptersNumber(); ++index) {
		addItemToList(index);
	}
	connect(ui.chapterList, SIGNAL(itemClicked(QListWidgetItem*)), SLOT(listItemClicked(QListWidgetItem*)));
	connect(ui.chapterList, SIGNAL(itemChanged(QListWidgetItem*)), SLOT(listItemChanged(QListWidgetItem*)));
}

void AdminInterface::initQuestionFrame() {
	ui.questionCompletionError->setVisible(false);
	ui.questionWidget->setVisible(false);
	initQuestionComboBox();
	for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
		singleChoiceButtons[index] = ui.centralWidget->findChild<QRadioButton*>(K::RADIO_BUTTON + QString::number(index + 1));
		multipleChoiceButtons[index] = ui.centralWidget->findChild<QCheckBox*>(K::CHECK_BOX + QString::number(index + 1));
		lineEditsRadio[index] = ui.centralWidget->findChild<QLineEdit*>(K::LINE_EDIT_RADIO + QString::number(index + 1));
		lineEditsCheck[index] = ui.centralWidget->findChild<QLineEdit*>(K::LINE_EDIT_CHECK + QString::number(index + 1));
	}
	initQuestionTableWidget();
	connect(ui.questionComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(questionComboBoxIndexChanged(int)));
	connect(ui.homeButton, SIGNAL(clicked()), this, SLOT(homeButtonClicked()));
	connect(ui.previousQuestion, SIGNAL(clicked()), this, SLOT(previousQuestionClicked()));
	connect(ui.nextQuestion, SIGNAL(clicked()), this, SLOT(nextQuestionClicked()));
	connect(ui.addRemoveQuestion, SIGNAL(clicked()), this, SLOT(addRemoveQuestionClicked()));
}

void AdminInterface::initQuestionComboBox() {
	ui.questionComboBox->addItem(K::SINGLE_CHOICE_QUESTION_TITLE);
	ui.questionComboBox->addItem(K::MULTIPLE_CHOICE_QUESTION_TITLE);
	ui.questionComboBox->addItem(K::INPUT_QUESTION_TITLE);
}

void AdminInterface::initQuestionTableWidget() {
	ui.questionTableWidget->setRowCount(0);
	ui.questionTableWidget->setColumnCount(K::MAXIMUM_COLUMNS_QUESTION_TABLE);
	ui.questionTableWidget->resizeColumnsToContents();
	ui.questionTableWidget->resizeRowsToContents();
	ui.questionTableWidget->setMinimumWidth(ui.questionTableWidget->columnWidth(0) * K::MAXIMUM_COLUMNS_QUESTION_TABLE +
		ui.questionTableWidget->columnWidth(0) / 3);
	ui.questionTableWidget->horizontalHeader()->setVisible(false);
	ui.questionTableWidget->verticalHeader()->setVisible(false);
	connect(ui.questionTableWidget, SIGNAL(cellClicked(int, int)), SLOT(tableItemClicked(int, int)));
}

void AdminInterface::updateChapterLayout() {
	ui.questionTableWidget->setRowCount(0);
	uint16_t questionsNumber = getChapterQuestionsNumber() + 1;
	uint16_t rowsDivide = questionsNumber / K::MAXIMUM_COLUMNS_QUESTION_TABLE;
	uint16_t rowsCount = questionsNumber % K::MAXIMUM_COLUMNS_QUESTION_TABLE == 0
		? rowsDivide
		: rowsDivide + 1;
	ui.questionTableWidget->setRowCount(rowsCount);
	uint16_t questionIndex;
	for (uint16_t indexRow = 0; indexRow < rowsCount; indexRow++) {
		for (uint8_t indexColumn = 0;
			(questionIndex = indexRow * K::MAXIMUM_COLUMNS_QUESTION_TABLE + indexColumn + 1)
			<= questionsNumber; ++indexColumn) {
			addTableItem(indexRow, indexColumn, questionIndex);
		}
	}
	currentQuestionIndex = questionsNumber - 1;
	ui.questionTableWidget->item(currentQuestionIndex / K::MAXIMUM_COLUMNS_QUESTION_TABLE,
		currentQuestionIndex % K::MAXIMUM_COLUMNS_QUESTION_TABLE)->setSelected(true);
	question = std::make_shared<SingleChoiceQuestion>(SingleChoiceQuestion(logger));
	updateQuestionLayout();
}

void AdminInterface::addTableItem(const uint16_t row, const uint8_t column, const uint16_t questionIndex) {
	QTableWidgetItem* questionTableItem = new QTableWidgetItem;
	questionTableItem->setText(QString::number(questionIndex));
	questionTableItem->setTextAlignment(Qt::AlignCenter);
	ui.questionTableWidget->setItem(row, column, questionTableItem);
}

void AdminInterface::uncheckSingleChoice(const uint8_t index) {
	singleChoiceButtons[index]->setAutoExclusive(false);
	singleChoiceButtons[index]->setChecked(false);
	singleChoiceButtons[index]->setAutoExclusive(true);
}

void AdminInterface::updateQuestionInfo() {
	std::shared_ptr<Question> currentQuestion = currentQuestionIndex == getChapterQuestionsNumber() ?
		question : questionBank.getChapter(currentChapterIndex).getQuestion(currentQuestionIndex);
	ui.questionTextEdit->setText(QString::fromStdString(currentQuestion->getQuestionText()));
	ui.questionTextEdit->setCursorPosition(0);
	ui.questionComboBox->blockSignals(true);
	comboBoxInput = false;
	if (std::shared_ptr<SingleChoiceQuestion> singleChoiceQuestion = std::dynamic_pointer_cast<SingleChoiceQuestion>(currentQuestion)) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
			lineEditsRadio[index]->setText(QString::fromStdString(singleChoiceQuestion->getQuestionAnswer(index)));
			lineEditsRadio[index]->setCursorPosition(0);
			uncheckSingleChoice(index);
		}
		if (singleChoiceQuestion->getCorrectAnswer() != UINT8_MAX) {
			singleChoiceButtons[singleChoiceQuestion->getCorrectAnswer()]->setChecked(true);
		}
		ui.questionPages->setCurrentIndex(K::SINGLE_CHOICE_INDEX);
		ui.questionComboBox->setCurrentIndex(K::SINGLE_CHOICE_INDEX);
	}
	else if (std::shared_ptr<MultipleChoiceQuestion> multipleChoiceQuestion = std::dynamic_pointer_cast<MultipleChoiceQuestion>(currentQuestion)) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
			lineEditsCheck[index]->setText(QString::fromStdString(multipleChoiceQuestion->getQuestionAnswer(index)));
			lineEditsCheck[index]->setCursorPosition(0);
			multipleChoiceButtons[index]->setChecked(multipleChoiceQuestion->getCorrectAnswers()[index]);
		}
		ui.questionPages->setCurrentIndex(K::MULTIPLE_CHOICE_INDEX);
		ui.questionComboBox->setCurrentIndex(K::MULTIPLE_CHOICE_INDEX);
	}
	else if (std::shared_ptr<InputQuestion> inputQuestion = std::dynamic_pointer_cast<InputQuestion>(currentQuestion)) {
		ui.inputLineEdit->setText(QString::fromStdString(inputQuestion->getCorrectAnswer()));
		ui.inputLineEdit->setCursorPosition(0);
		ui.questionPages->setCurrentIndex(K::INPUT_INDEX);
		ui.questionComboBox->setCurrentIndex(K::INPUT_INDEX);
		comboBoxInput = true;
	}
	ui.questionComboBox->blockSignals(false);
	if (currentQuestionIndex == getChapterQuestionsNumber()) {
		ui.addRemoveQuestion->setText(K::ADD_QUESTION);
	}
	else {
		ui.addRemoveQuestion->setText(K::REMOVE_QUESTION);
	}
}

void AdminInterface::updateQuestionLayout() {
	setTableItemSelected(currentQuestionIndex, true);
	if (currentQuestionIndex == 0) {
		ui.previousQuestion->setEnabled(false);
	}
	else {
		ui.previousQuestion->setEnabled(true);
	}
	if (currentQuestionIndex == getChapterQuestionsNumber()) {
		ui.nextQuestion->setEnabled(false);
	}
	else {
		ui.nextQuestion->setEnabled(true);
	}
	updateQuestionInfo();
}

bool AdminInterface::saveQuestion(bool addLastQuestion) {
	bool chapterQuestion = currentQuestionIndex < getChapterQuestionsNumber() || addLastQuestion;
	std::string questionText = ui.questionTextEdit->text().toStdString();
	if (questionText.empty() && chapterQuestion) {
		showCompletionError(K::QUESTION_TEXT_NOT_COMPLETED);
		return false;
	}
	switch (ui.questionComboBox->currentIndex()) {
	case K::SINGLE_CHOICE_INDEX:
		return saveSingleChoiceQuestion(questionText, chapterQuestion, addLastQuestion);
	case K::MULTIPLE_CHOICE_INDEX:
		return saveMultipleChoiceQuestion(questionText, chapterQuestion, addLastQuestion);
	case K::INPUT_INDEX:
		return saveInputQuestion(questionText, chapterQuestion, addLastQuestion);
	}
	return true;
}

bool AdminInterface::saveSingleChoiceQuestion(const std::string& questionText, const bool chapterQuestion, const bool addLastQuestion) {
	std::array<std::string, ChoiceQuestion::ANSWERS_NUMBER> answers = { "", "", "", "" };
	uint8_t correctAnswer = UINT8_MAX;
	for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
		answers[index] = lineEditsRadio[index]->text().toStdString();
		if (chapterQuestion && answers[index].empty()) {
			showCompletionError(K::ANSWERS_NOT_COMPLETED);
			return false;
		}
		if (singleChoiceButtons[index]->isChecked()) {
			correctAnswer = index;
		}
	}
	if (chapterQuestion && correctAnswer == UINT8_MAX) {
		showCompletionError(K::CORRECT_ANSWER_NOT_SELECTED);
		return false;
	}
	if (chapterQuestion && !checkDuplicatedAnswers(lineEditsRadio)) {
		showCompletionError(K::DUPLICATED_ANSWERS);
		return false;
	}
	std::shared_ptr<Question> newQuestion = std::make_shared<SingleChoiceQuestion>(SingleChoiceQuestion(questionText, answers, correctAnswer, logger));
	logger->log(K::LOG::CREATED_SINGLE_CHOICE_QUESTION, Logger::Level::INFO);
	overrideQuestion(chapterQuestion, addLastQuestion, newQuestion);
}

bool AdminInterface::saveMultipleChoiceQuestion(const std::string& questionText, const bool chapterQuestion, const bool addLastQuestion) {
	std::array<std::string, ChoiceQuestion::ANSWERS_NUMBER> answers = { "", "", "", "" };
	std::array<bool, ChoiceQuestion::ANSWERS_NUMBER> correctAnswers = { false, false, false, false };
	bool answerSelected = false;
	for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
		answers[index] = lineEditsCheck[index]->text().toStdString();
		if (chapterQuestion && answers[index].empty()) {
			showCompletionError(K::ANSWERS_NOT_COMPLETED);
			return false;
		}
		correctAnswers[index] = multipleChoiceButtons[index]->isChecked();
		if (correctAnswers[index]) {
			answerSelected = true;
		}
	}
	if (chapterQuestion && !answerSelected) {
		showCompletionError(K::CORRECT_ANSWERS_NOT_SELECTED);
		return false;
	}
	if (chapterQuestion && !checkDuplicatedAnswers(lineEditsCheck)) {
		showCompletionError(K::DUPLICATED_ANSWERS);
		return false;
	}
	std::shared_ptr<Question> newQuestion = std::make_shared<MultipleChoiceQuestion>(MultipleChoiceQuestion(questionText, answers, correctAnswers, logger));
	logger->log(K::LOG::CREATED_MULTIPLE_CHOICE_QUESTION, Logger::Level::INFO);
	overrideQuestion(chapterQuestion, addLastQuestion, newQuestion);
}

bool AdminInterface::saveInputQuestion(const std::string& questionText, const bool chapterQuestion, const bool addLastQuestion) {
	std::string correctAnswer = ui.inputLineEdit->text().toStdString();
	if (chapterQuestion && correctAnswer.empty()) {
		showCompletionError(K::CORRECT_ANSWER_NOT_COMPLETED);
		return false;
	}
	std::shared_ptr<Question> newQuestion = std::make_shared<InputQuestion>(InputQuestion(questionText, correctAnswer));
	logger->log(K::LOG::CREATED_INPUT_QUESTION, Logger::Level::INFO);
	overrideQuestion(chapterQuestion, addLastQuestion, newQuestion);
}

void AdminInterface::overrideQuestion(const bool chapterQuestion, const bool addLastQuestion, const std::shared_ptr<Question>& newQuestion) {
	chapterQuestion && !addLastQuestion ? questionBank.getChapter(currentChapterIndex).setQuestion(currentQuestionIndex, newQuestion) : question = newQuestion;
	ui.questionCompletionError->setVisible(false);
}

void AdminInterface::showCompletionError(const QString& error) {
	ui.questionCompletionError->setText(error);
	ui.questionCompletionError->setVisible(true);
}

bool AdminInterface::checkDuplicatedAnswers(const std::array<QLineEdit*, ChoiceQuestion::ANSWERS_NUMBER>& answers) const {
	std::set<QString> answersSet;
	std::for_each(answers.begin(), answers.end(), [&answersSet](const QLineEdit* qLineEdit) {
		answersSet.insert(qLineEdit->text());
		});
	return answersSet.size() == ChoiceQuestion::ANSWERS_NUMBER;
}

void AdminInterface::setTableItemSelected(uint16_t index, bool selected) {
	if (index < getChapterQuestionsNumber() + 1) {
		ui.questionTableWidget->item(index / K::MAXIMUM_COLUMNS_QUESTION_TABLE,
			index % K::MAXIMUM_COLUMNS_QUESTION_TABLE)->setSelected(selected);
	}
}

void AdminInterface::addItemToList(const uint16_t index) {
	QListWidgetItem* chapterListItem = new QListWidgetItem;
	chapterListItem->setFlags(chapterListItem->flags() | Qt::ItemIsEditable);
	chapterListItem->setText(QString::fromStdString(questionBank.getFormattedChapter(index)) + getChapterItemSuffix(index));
	chapterListItem->setTextColor(K::WHITE);
	ui.chapterList->addItem(chapterListItem);
}

void AdminInterface::previousNextQuestionClicked(bool previous) {
	if (!saveQuestion()) {
		return;
	}
	setTableItemSelected(currentQuestionIndex, false);
	if (previous) {
		--currentQuestionIndex;
	}
	else {
		++currentQuestionIndex;
	}
	updateQuestionLayout();
}

void AdminInterface::updateTableWidget(bool add) {
	uint16_t questionsNumber = getChapterQuestionsNumber();
	if (!add) {
		++questionsNumber;
	}
	uint16_t rows = questionsNumber / K::MAXIMUM_COLUMNS_QUESTION_TABLE;
	uint8_t rowsRemainder = questionsNumber % K::MAXIMUM_COLUMNS_QUESTION_TABLE;
	if (add) {
		if (rowsRemainder == 0) {
			ui.questionTableWidget->setRowCount(rows + 1);
		}
		addTableItem(rows, rowsRemainder == 0 ? 0 : rowsRemainder, currentQuestionIndex + 1);
		return;
	}
	if (rowsRemainder == 1) {
		ui.questionTableWidget->setRowCount(rows);
		return;
	}
	delete ui.questionTableWidget->takeItem(rowsRemainder == 0 ? rows - 1 : rows, rowsRemainder == 0 ? K::MAXIMUM_COLUMNS_QUESTION_TABLE - 1 : rowsRemainder - 1);
}

void AdminInterface::buttonClicked(const QString& buttonName) {
	logger->log(buttonName.toStdString() + K::LOG::PRESSED, Logger::Level::INFO);
}

inline const uint16_t AdminInterface::getChapterQuestionsNumber() const {
	return questionBank.getChapter(currentChapterIndex).getQuestionsNumber();
}

inline const QString AdminInterface::getChapterItemSuffix(const uint16_t index) const {
	return questionBank.getChapter(index).getQuestionsNumber() == 1 ? K::QUESTION : K::QUESTIONS;
}

bool AdminInterface::eventFilter(QObject* obj, QEvent* event) {
	if (event->type() == QEvent::KeyPress) {
		QKeyEvent* key = static_cast<QKeyEvent*>(event);
		if ((key->key() == Qt::Key_Enter) || (key->key() == Qt::Key_Return)
			&& ui.chapterLineEdit->text().length() > K::MINIMUM_CHAPTER_NAME_SIZE && chaptersLoaded) {
			QApplication::focusWidget()->clearFocus();
			addChapterClicked();
			return true;
		}
	}
	return QObject::eventFilter(obj, event);
}

void AdminInterface::closeEvent(QCloseEvent* event) {
	if (ui.questionWidget->isVisible() && currentQuestionIndex < getChapterQuestionsNumber()) {
		saveQuestion();
	}
	std::stringstream ss;
	questionBank.saveQuestions(ss);
	ss.seekg(std::ios_base::beg);
	SimpleCrypt crypto(K::ENCRYPTION_KEY);
	std::string line;
	std::ofstream output(fileName);
	while (std::getline(ss, line)) {
		QString result = crypto.encryptToString(QString::fromStdString(line));
		output << result.toStdString() << std::endl;
	}
	output.close();
	loggerStream.close();
}

void AdminInterface::showDeleteOption(const QPoint& point) {
	QPoint globalPoint = ui.chapterList->mapToGlobal(point);
	QMenu menu;
	menu.addAction(K::ERASE_CHAPTER, this, SLOT(eraseChapter()));
	menu.addAction(K::RENAME_CHAPTER, this, SLOT(renameChapter()));
	menu.exec(globalPoint);
}

void AdminInterface::eraseChapter() {
	buttonClicked(K::LOG::ERASE_CHAPTER);
	int currentIndex = ui.chapterList->currentRow();
	questionBank.deleteChapter(currentIndex);
	delete ui.chapterList->takeItem(currentIndex);
}

void AdminInterface::renameChapter() {
	uint16_t index = ui.chapterList->currentRow();
	QListWidgetItem* item = ui.chapterList->item(index);
	item->setText(QString::fromStdString(questionBank.getChapter(index).getChapterName()));
	isEditing = true;
	ui.chapterList->editItem(item);
}

void AdminInterface::listItemClicked(QListWidgetItem* item) {
	currentChapterIndex = ui.chapterList->currentIndex().row();
	buttonClicked(K::LOG::LIST_ITEM + QString::number(currentChapterIndex + 1));
	updateChapterLayout();
	ui.mainWidget->setVisible(false);
	ui.questionWidget->setVisible(true);
}

void AdminInterface::listItemChanged(QListWidgetItem* item) {
	if (!isEditing) {
		return;
	}
	isEditing = false;
	uint16_t index = ui.chapterList->currentRow();
	questionBank.getChapter(index).setChapterName(item->text().toStdString());
	item->setText(QString::fromStdString(questionBank.getFormattedChapter(index)) + getChapterItemSuffix(index));
}

void AdminInterface::textEntered(const QString& text) {
	ui.addChapter->setEnabled(text.length() > K::MINIMUM_CHAPTER_NAME_SIZE && chaptersLoaded);
}

void AdminInterface::addChapterClicked() {
	questionBank.addChapter(ui.chapterLineEdit->text().toStdString());
	addItemToList(questionBank.getChaptersNumber() - 1);
	ui.chapterLineEdit->setText(QString());
	ui.addChapter->setEnabled(false);
	buttonClicked(K::LOG::ADD_CHAPTER);
}

void AdminInterface::homeButtonClicked() {
	if (currentQuestionIndex < getChapterQuestionsNumber() && !saveQuestion()) {
		return;
	}
	ui.chapterList->currentItem()->setText(QString::fromStdString(questionBank.getFormattedChapter(currentChapterIndex)) + getChapterItemSuffix(currentChapterIndex));
	ui.questionWidget->setVisible(false);
	ui.mainWidget->setVisible(true);
	buttonClicked(K::LOG::HOME_BUTTON);
}

void AdminInterface::questionComboBoxIndexChanged(int index) {
	switch (index) {
	case K::SINGLE_CHOICE_INDEX:
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
			lineEditsRadio[index]->setText(comboBoxInput ? QString() : lineEditsCheck[index]->text());
			lineEditsRadio[index]->setCursorPosition(0);
			uncheckSingleChoice(index);
		}
		comboBoxInput = false;
		break;
	case K::MULTIPLE_CHOICE_INDEX:
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
			lineEditsCheck[index]->setText(comboBoxInput ? QString() : lineEditsRadio[index]->text());
			lineEditsCheck[index]->setCursorPosition(0);
			multipleChoiceButtons[index]->setChecked(false);
		}
		comboBoxInput = false;
		break;
	case K::INPUT_INDEX:
		ui.inputLineEdit->setText(QString());
		comboBoxInput = true;
		break;
	}
	ui.questionPages->setCurrentIndex(index);
	buttonClicked(K::LOG::COMBO_BOX_ITEM + QString::number(index + 1));
}

void AdminInterface::tableItemClicked(int row, int column) {
	if (row * K::MAXIMUM_COLUMNS_QUESTION_TABLE + column > getChapterQuestionsNumber()) {
		return;
	}
	if (!saveQuestion()) {
		return;
	}
	currentQuestionIndex = row * K::MAXIMUM_COLUMNS_QUESTION_TABLE + column;
	buttonClicked(K::LOG::TABLE_ITEM + QString::number(currentQuestionIndex + 1));
	updateQuestionLayout();
}

void AdminInterface::previousQuestionClicked() {
	buttonClicked(K::LOG::PREVIOUS_QUESTION);
	previousNextQuestionClicked(true);
}

void AdminInterface::nextQuestionClicked() {
	buttonClicked(K::LOG::NEXT_QUESTION);
	previousNextQuestionClicked(false);
}

void AdminInterface::addRemoveQuestionClicked() {
	setTableItemSelected(currentQuestionIndex, false);
	Chapter& chapter = questionBank.getChapter(currentChapterIndex);
	if (currentQuestionIndex == getChapterQuestionsNumber()) {
		if (!saveQuestion(true)) {
			return;
		}
		chapter.addQuestion(question);
		questionBank.incrementDecrement(true);
		question = std::make_shared<SingleChoiceQuestion>(SingleChoiceQuestion(logger));
		++currentQuestionIndex;
		updateQuestionInfo();
		updateTableWidget(true);
		setTableItemSelected(currentQuestionIndex, true);
		buttonClicked(K::LOG::ADD_QUESTION);
		return;
	}
	buttonClicked(K::LOG::REMOVE_QUESTION);
	updateTableWidget(false);
	chapter.deleteQuestion(currentQuestionIndex);
	questionBank.incrementDecrement(false);
	updateQuestionLayout();
	ui.questionCompletionError->setVisible(false);
}
