#include <QtWidgets/QApplication>

#include "AdminInterface.h"

int main(int argc, char* argv[]) {
	QApplication app(argc, argv);
	AdminInterface adminInterface("../input/input.txt");
	adminInterface.show();
	return app.exec();
}