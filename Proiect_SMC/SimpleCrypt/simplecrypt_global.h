#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(SIMPLECRYPT_LIB)
#  define SIMPLECRYPT_EXPORT Q_DECL_EXPORT
# else
#  define SIMPLECRYPT_EXPORT Q_DECL_IMPORT
# endif
#else
# define SIMPLECRYPT_EXPORT
#endif
