#include "pch.h"

#include <fstream>

#include "CppUnitTest.h"
#include "../QuestionnaireFramework/Question.h"
#include "../QuestionnaireFramework/Question.cpp"
#include "../QuestionnaireFramework/ChoiceQuestion.h"
#include "../QuestionnaireFramework/ChoiceQuestion.cpp"
#include "../QuestionnaireFramework/SingleChoiceQuestion.h"
#include "../QuestionnaireFramework/SingleChoiceQuestion.cpp"

using Microsoft::VisualStudio::CppUnitTestFramework::Assert;

namespace SingleChoiceQuestionTest {
	TEST_CLASS(SingleChoiceQuestionTest) {
		const std::string UNIT_TEST_LOG_FILE = "../output/unit_test_log.txt";
		std::ofstream loggerStream = std::ofstream(UNIT_TEST_LOG_FILE);
		std::shared_ptr<Logger> logger = std::make_shared<Logger>(loggerStream);
		std::array<std::string, ChoiceQuestion::ANSWERS_NUMBER> answers = { "Rasp1", "Rasp2", "Rasp3", "Rasp4" };
		std::string questionText = "Intrebare";
		uint8_t correctAnswer = 0;

public:
	TEST_METHOD(Constructor) {
		SingleChoiceQuestion question(questionText, answers, correctAnswer, logger);
		Assert::IsTrue(isEqual(question, questionText, answers, correctAnswer));
	}

	TEST_METHOD(CopyConstructor) {
		SingleChoiceQuestion question1(questionText, answers, correctAnswer, logger);
		SingleChoiceQuestion question2 = question1;
		Assert::IsTrue(isEqual(question1, question2));
	}

	TEST_METHOD(AssignmentOperator) {
		SingleChoiceQuestion question1(questionText, answers, correctAnswer, logger);
		SingleChoiceQuestion question2(logger);
		question2 = question1;
		Assert::IsTrue(isEqual(question1, question2));
	}

	TEST_METHOD(MoveConstructor) {
		SingleChoiceQuestion question1(questionText, answers, correctAnswer, logger);
		SingleChoiceQuestion question2(std::move(question1));
		Assert::IsTrue(isEqual(question2, questionText, answers, correctAnswer));
		Assert::IsTrue(testMoveSemantics(question1));
	}

	TEST_METHOD(MoveAssignmentOperator) {
		SingleChoiceQuestion question1(questionText, answers, correctAnswer, logger);
		SingleChoiceQuestion question2(logger);
		question2 = std::move(question1);
		Assert::IsTrue(isEqual(question2, questionText, answers, correctAnswer));
		Assert::IsTrue(testMoveSemantics(question1));
	}

	TEST_METHOD(CheckAnswer) {
		SingleChoiceQuestion question(questionText, answers, correctAnswer, logger);
		Assert::IsTrue(question.checkAnswer(std::to_string(correctAnswer)));
	}

private:
	bool isEqual(const SingleChoiceQuestion& question, const std::string& questionText, const std::array<std::string, ChoiceQuestion::ANSWERS_NUMBER>& answers, uint8_t correctAnswer) {
		return (question.getQuestionText() == questionText &&
			testAnswers(question, answers) &&
			question.getCorrectAnswer() == correctAnswer);
	}

	bool isEqual(const SingleChoiceQuestion& question1, const SingleChoiceQuestion& question2) {
		return (question1.getQuestionText() == question2.getQuestionText() &&
			testAnswers(question1, question2) &&
			question1.getCorrectAnswer() == question2.getCorrectAnswer());
	}

	bool testAnswers(const SingleChoiceQuestion& question, const std::array<std::string, ChoiceQuestion::ANSWERS_NUMBER>& answers) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index)
			if (question.getQuestionAnswer(index) != answers[index])
				return false;
		return true;
	}

	bool testAnswers(const SingleChoiceQuestion& question1, const SingleChoiceQuestion& question2) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index)
			if (question1.getQuestionAnswer(index) != question2.getQuestionAnswer(index))
				return false;
		return true;
	}

	bool testMoveSemantics(const SingleChoiceQuestion& question) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
			if (!question.getQuestionAnswer(index).empty())
				return false;
		}
		return question.getQuestionText().empty();
	}
	};
}