#include "pch.h"

#include <fstream>

#include "CppUnitTest.h"
#include "../QuestionnaireFramework/MultipleChoiceQuestion.h"
#include "../QuestionnaireFramework/MultipleChoiceQuestion.cpp"

using Microsoft::VisualStudio::CppUnitTestFramework::Assert;

namespace MultipleChoiceQuestionTest {

	TEST_CLASS(MultipleChoiceQuestionTest) {
		const std::string UNIT_TEST_LOG_FILE = "../output/unit_test_log.txt";
		std::ofstream loggerStream = std::ofstream(UNIT_TEST_LOG_FILE);
		std::shared_ptr<Logger> logger = std::make_shared<Logger>(loggerStream);
		std::array<std::string, ChoiceQuestion::ANSWERS_NUMBER> answers = { "Rasp1", "Rasp2", "Rasp3", "Rasp4" };
		std::string questionText = "Intrebare";
		std::array<bool, ChoiceQuestion::ANSWERS_NUMBER> correctAnswers = { true, false, true, false };

public:
	TEST_METHOD(Constructor) {
		MultipleChoiceQuestion question(questionText, answers, correctAnswers, logger);
		Assert::IsTrue(question.getQuestionText() == questionText);
		Assert::IsTrue(testAnswers(question, answers));
		Assert::IsTrue(question.getCorrectAnswers() == correctAnswers);
	}

	TEST_METHOD(CopyConstructor) {
		MultipleChoiceQuestion question1(questionText, answers, correctAnswers, logger);
		MultipleChoiceQuestion question2 = question1;
		Assert::IsTrue(isEqual(question1, question2));
	}

	TEST_METHOD(AssignmentOperator) {
		MultipleChoiceQuestion question1(questionText, answers, correctAnswers, logger);
		MultipleChoiceQuestion question2(logger);
		question2 = question1;
		Assert::IsTrue(isEqual(question1, question2));
	}

	TEST_METHOD(MoveConstructor) {
		MultipleChoiceQuestion question1(questionText, answers, correctAnswers, logger);
		MultipleChoiceQuestion question2(std::move(question1));
		Assert::IsTrue(isEqual(question2, questionText, answers, correctAnswers));
		Assert::IsTrue(testMoveSemantics(question1));
	}

	TEST_METHOD(MoveAssignmentOperator) {
		MultipleChoiceQuestion question1(questionText, answers, correctAnswers, logger);
		MultipleChoiceQuestion question2(logger);
		question2 = std::move(question1);
		Assert::IsTrue(isEqual(question2, questionText, answers, correctAnswers));
		Assert::IsTrue(testMoveSemantics(question1));
	}

	TEST_METHOD(CheckAnswer) {
		std::ostringstream stream;
		MultipleChoiceQuestion question(questionText, answers, correctAnswers, logger);
		for (bool answer : correctAnswers)
			stream << answer;
		Assert::IsTrue(question.checkAnswer(stream.str()));
	}

private:
	bool isEqual(const MultipleChoiceQuestion& question, const std::string& questionText, const std::array<std::string, ChoiceQuestion::ANSWERS_NUMBER>& answers, const std::array<bool, ChoiceQuestion::ANSWERS_NUMBER>& correctAnswers) {
		return (question.getQuestionText() == questionText &&
			testAnswers(question, answers) &&
			question.getCorrectAnswers() == correctAnswers);
	}

	bool isEqual(const MultipleChoiceQuestion& question1, const MultipleChoiceQuestion& question2) {
		return (question1.getQuestionText() == question2.getQuestionText() &&
			testAnswers(question1, question2) &&
			question1.getCorrectAnswers() == question2.getCorrectAnswers());
	}

	bool testAnswers(const MultipleChoiceQuestion& question, const std::array<std::string, ChoiceQuestion::ANSWERS_NUMBER>& answers) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index)
			if (question.getQuestionAnswer(index) != answers[index])
				return false;
		return true;
	}

	bool testAnswers(const MultipleChoiceQuestion& question1, const MultipleChoiceQuestion& question2) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index)
			if (question1.getQuestionAnswer(index) != question2.getQuestionAnswer(index))
				return false;
		return true;
	}
	bool testMoveSemantics(const MultipleChoiceQuestion& question) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
			if (!question.getQuestionAnswer(index).empty())
				return false;
		}
		return question.getQuestionText().empty();
	}
	};
}