#include "pch.h"

#include <fstream>

#include "CppUnitTest.h"
#include "../QuestionnaireFramework/Chapter.h"
#include "../QuestionnaireFramework/Chapter.cpp"

using Microsoft::VisualStudio::CppUnitTestFramework::Assert;

namespace ChapterTests {
	TEST_CLASS(ChapterTest) {
		std::string stream_aux = "4\n"
			"SingleChoice\n"
			"Intrebare ? \n"
			"Raspuns1\n"
			"Raspuns2\n"
			"Raspuns3\n"
			"Raspuns4\n"
			"0\n"
			"Input\n"
			"Intrebare ? \n"
			"Raspuns\n"
			"MultipleChoice\n"
			"Intrebare ? \n"
			"Raspuns1\n"
			"Raspuns2\n"
			"Raspuns3\n"
			"Raspuns4\n"
			"0 1 2\n"
			"Input\n"
			"Intrebare ? \n"
			"Raspuns\n"
			"End of Chapter";

		const std::string CHAPTER_NAME = "Capitol";
		const std::string UNIT_TEST_LOG_FILE = "../output/unit_test_log.txt";
		std::ofstream loggerStream = std::ofstream(UNIT_TEST_LOG_FILE);
		std::shared_ptr<Logger> logger = std::make_shared<Logger>(loggerStream);

public:
	TEST_METHOD(ReadOperator) {
		Chapter chapter(CHAPTER_NAME, logger);
		uint16_t size = UINT16_MAX;
		readChapter(chapter, size);
		Assert::AreEqual(int(chapter.getQuestionsNumber()), int(size));
	}

	TEST_METHOD(CopyConstructor) {
		Chapter chapter1(CHAPTER_NAME, logger);
		uint16_t size = UINT16_MAX;
		readChapter(chapter1, size);
		Chapter chapter2(chapter1);
		Assert::AreEqual(int(chapter1.getQuestionsNumber()), int(size));
		Assert::AreEqual(int(chapter2.getQuestionsNumber()), int(size));
	}

	TEST_METHOD(CopyOperator) {
		Chapter chapter1(CHAPTER_NAME, logger), chapter2(CHAPTER_NAME, logger);
		uint16_t size = UINT16_MAX;
		readChapter(chapter1, size);
		chapter2 = chapter1;
		Assert::AreEqual(int(chapter1.getQuestionsNumber()), int(size));
		Assert::AreEqual(int(chapter2.getQuestionsNumber()), int(size));
	}

	TEST_METHOD(MoveConstructor) {
		Chapter chapter1(CHAPTER_NAME, logger);
		uint16_t size = UINT16_MAX;
		readChapter(chapter1, size);
		Chapter chapter2(std::move(chapter1));
		Assert::AreEqual(int(chapter1.getQuestionsNumber()), 0);
		Assert::AreEqual(int(chapter2.getQuestionsNumber()), int(size));
	}

	TEST_METHOD(MoveOperator) {
		Chapter chapter1(CHAPTER_NAME, logger), chapter2(CHAPTER_NAME, logger);
		uint16_t size = UINT16_MAX;
		readChapter(chapter1, size);
		chapter2 = std::move(chapter1);
		Assert::AreEqual(int(chapter1.getQuestionsNumber()), 0);
		Assert::AreEqual(int(chapter2.getQuestionsNumber()), int(size));
	}

private:
	void readChapter(Chapter& chapter, uint16_t& size) {
		std::stringstream stream;
		stream << stream_aux;
		stream >> size;
		stream >> chapter;
	}
	};
}