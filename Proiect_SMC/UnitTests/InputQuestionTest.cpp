#include "pch.h"

#include <fstream>

#include "CppUnitTest.h"
#include "../QuestionnaireFramework/InputQuestion.h"
#include "../QuestionnaireFramework/InputQuestion.cpp"

using Microsoft::VisualStudio::CppUnitTestFramework::Assert;

namespace InputQuestionTest {

	TEST_CLASS(InputQuestionTest) {
		std::string questionText = "Intrebare";
		std::string answerText = "Raspuns";

public:
	TEST_METHOD(Constructor) {
		InputQuestion question(questionText, answerText);
		Assert::IsTrue(question.getQuestionText() == questionText);
		Assert::IsTrue(question.getCorrectAnswer() == answerText);
	}

	TEST_METHOD(CopyConstructor) {
		InputQuestion question1(questionText, answerText);
		InputQuestion question2 = question1;
		Assert::IsTrue(isEqual(question1, question2));
	}

	TEST_METHOD(AssignmentOperator) {
		InputQuestion question1(questionText, answerText);
		InputQuestion question2;
		question2 = question1;
		Assert::IsTrue(isEqual(question1, question2));
	}

	TEST_METHOD(MoveConstructor) {
		InputQuestion question1(questionText, answerText);
		InputQuestion question2(std::move(question1));
		Assert::IsTrue(isEqual(question2, questionText, answerText));
		Assert::IsTrue(testMoveSemantics(question1));
	}

	TEST_METHOD(MoveAssignmentOperator) {
		InputQuestion question1(questionText, answerText);
		InputQuestion question2;
		question2 = std::move(question1);
		Assert::IsTrue(isEqual(question2, questionText, answerText));
		Assert::IsTrue(testMoveSemantics(question1));
	}

	TEST_METHOD(CheckAnswer) {
		InputQuestion question(questionText, answerText);
		Assert::IsTrue(question.checkAnswer(answerText));
	}

private:
	bool isEqual(const InputQuestion& question, const std::string& questionText, const std::string& answerText) {
		return (question.getQuestionText() == questionText &&
			question.getCorrectAnswer() == answerText);
	}

	bool isEqual(const InputQuestion& question1, const InputQuestion& question2) {
		return (question1.getQuestionText() == question2.getQuestionText() &&
			question1.getCorrectAnswer() == question2.getCorrectAnswer());
	}

	bool testMoveSemantics(const InputQuestion& question) {
		return (question.getCorrectAnswer().empty() &&
			question.getCorrectAnswer().empty());
	}
	};
}