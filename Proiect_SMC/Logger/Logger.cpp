#include <chrono>
#include <iomanip>

#include "Logger.h"

const std::string INFO_STRING = "Info";
const std::string WARNING_STRING = "Warning";
const std::string ERROR_STRING = "Error";

Logger::Logger(std::ostream& out, Level minimumLevel)
	: out(out), minimumLevel(minimumLevel) {}

void Logger::log(const std::string& message, Level level) {
	log(message.c_str(), level);
}

void Logger::log(const char* message, Level level) {
	if (static_cast<int>(level) < static_cast<int>(minimumLevel)) {
		return;
	}
	if (static_cast<int>(level) >= static_cast<int>(Level::COUNT)) {
		return;
	}
	auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	struct tm time;
	localtime_s(&time, &now);
	out << std::put_time(&time, "[%H:%M:%S %d.%m.%Y]") << "[" << loggerLevelToString(level) << "]: " << message << std::endl;
}

std::string loggerLevelToString(Logger::Level level) {
	switch (level) {
	case Logger::Level::INFO:
		return INFO_STRING;
	case Logger::Level::WARNING:
		return WARNING_STRING;
	case Logger::Level::ERROR:
		return ERROR_STRING;
	case Logger::Level::COUNT:
	default:
		return "";
	}
}