#pragma once
#include <iostream>

#ifdef LOGGER_EXPORTS
#define LOGGING_API __declspec(dllexport)
#else
# define LOGGING_API __declspec(dllimport)
#endif

class LOGGING_API Logger {
public:
	enum class Level {
		INFO,
		WARNING,
		ERROR,
		COUNT
	};

private:
	std::ostream& out;
	Level minimumLevel;

	static const std::string INFO_STRING;
	static const std::string WARNING_STRING;
	static const std::string ERROR_STRING;

public:
	Logger(std::ostream& out, Level minimumLevel = Level::INFO);
	void log(const std::string& message, Level level = Level::INFO);
	void log(const char* message, Level level = Level::INFO);
};

std::string loggerLevelToString(Logger::Level level);