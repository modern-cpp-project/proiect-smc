#pragma once

#include <array>

#include "Question.h"

class __declspec(dllexport) ChoiceQuestion : public Question {
public:
	static const uint8_t ANSWERS_NUMBER = 4;

protected:
	static const std::string DUPLICATED_ANSWERS_ERROR;
	static const std::string CORRECT_ANSWER_INVALID;
	std::array<std::string, ANSWERS_NUMBER> answers;

	ChoiceQuestion() = default;

	void read(std::istream& input);
	void write(std::ostream& output) const;
public:
	ChoiceQuestion(const std::string& questionText, const std::array<std::string, ANSWERS_NUMBER>& answers);
	virtual ~ChoiceQuestion() = default;
	ChoiceQuestion(const ChoiceQuestion& other);
	ChoiceQuestion(ChoiceQuestion&& other);
	ChoiceQuestion& operator=(const ChoiceQuestion& other);
	ChoiceQuestion& operator=(ChoiceQuestion&& other);
	const std::string& getQuestionAnswer(uint8_t index) const noexcept;
};

