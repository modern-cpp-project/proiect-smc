#pragma once

#include "../Logger/Logger.h"

#include "ChoiceQuestion.h"

class __declspec(dllexport) SingleChoiceQuestion : public ChoiceQuestion {
private:
	uint8_t correctAnswer;
	static const std::string REGEX_CORRECT_ANSWER;
	std::shared_ptr<Logger> logger;

protected:
	void read(std::istream& input);
	void write(std::ostream& output) const;

public:
	SingleChoiceQuestion(std::shared_ptr<Logger> logger);
	SingleChoiceQuestion(const std::string& questionText, const std::array<std::string, ANSWERS_NUMBER>& answers, const uint8_t correctAnswer, std::shared_ptr<Logger> logger);
	~SingleChoiceQuestion() = default;
	SingleChoiceQuestion(const SingleChoiceQuestion& other);
	SingleChoiceQuestion(SingleChoiceQuestion&& other);
	SingleChoiceQuestion& operator=(const SingleChoiceQuestion& other);
	SingleChoiceQuestion& operator=(SingleChoiceQuestion&& other);
	friend std::istream& operator>>(std::istream& input, SingleChoiceQuestion& inputQuestion);
	const uint8_t getCorrectAnswer() const noexcept;
	float checkAnswer(const std::string& userAnswer) const;
	std::shared_ptr<Question> copy() const;
};