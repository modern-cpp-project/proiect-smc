#pragma once

#include <vector>

#include "../Logger/Logger.h"

#include "Chapter.h"
#include "Questionnaire.h"

class __declspec(dllexport) QuestionBank {
private:
	std::vector<Chapter> chapters;
	uint32_t questionsNumber = 0;
	std::shared_ptr<Logger> logger;

	static const std::string LOADED_QUESTIONS;

public:
	QuestionBank() = default;
	QuestionBank(std::istream& ist, std::shared_ptr<Logger> logger);
	~QuestionBank() = default;
	QuestionBank(const QuestionBank& other);
	QuestionBank(QuestionBank&& other);
	QuestionBank& operator=(const QuestionBank& other);
	QuestionBank& operator=(QuestionBank&& other);
	Questionnaire createQuestionnaire(const uint16_t questionnaireSize);
	const uint32_t getQuestionsNumber() const;
	const uint16_t getChaptersNumber() const;
	Chapter& getChapter(const uint16_t index);
	const Chapter& getChapter(const uint16_t index) const;
	const std::string getFormattedChapter(const uint16_t index) const;
	void addChapter(const std::string& chapterName);
	void deleteChapter(const int index);
	void saveQuestions(std::ostream& ost) const;
	void incrementDecrement(bool increment);
};

