#include <iostream>
#include <regex>

#include "SingleChoiceQuestion.h"

const std::string SingleChoiceQuestion::REGEX_CORRECT_ANSWER = R"(0|1|2|3)";

void SingleChoiceQuestion::read(std::istream& input) {
	ChoiceQuestion::read(input);
	std::string answer;
	std::regex correctAnswersFormat{ REGEX_CORRECT_ANSWER };
	if (!(input >> answer && std::regex_match(answer, correctAnswersFormat))) {
		logger->log(SingleChoiceQuestion::CORRECT_ANSWER_INVALID, Logger::Level::ERROR);
		throw SingleChoiceQuestion::CORRECT_ANSWER_INVALID;
	}
	uint16_t correctAnswerInput = std::stoi(answer);
	correctAnswer = static_cast<uint8_t>(correctAnswerInput);
	input.get();
}

void SingleChoiceQuestion::write(std::ostream& output) const {
	ChoiceQuestion::write(output);
	output << unsigned(correctAnswer) << std::endl;
}

SingleChoiceQuestion::SingleChoiceQuestion(std::shared_ptr<Logger> logger)
	: correctAnswer(UINT8_MAX), logger(logger) {}

SingleChoiceQuestion::SingleChoiceQuestion(const std::string& questionText, const std::array<std::string, ANSWERS_NUMBER>& answers, const uint8_t correctAnswer, std::shared_ptr<Logger> logger)
	: ChoiceQuestion(questionText, answers), correctAnswer(correctAnswer), logger(logger) {}

SingleChoiceQuestion::SingleChoiceQuestion(const SingleChoiceQuestion& other)
	: ChoiceQuestion(other.questionText, other.answers), correctAnswer(other.correctAnswer), logger(other.logger) {}

SingleChoiceQuestion::SingleChoiceQuestion(SingleChoiceQuestion&& other) {
	*this = std::forward<SingleChoiceQuestion&&>(other);
}

SingleChoiceQuestion& SingleChoiceQuestion::operator=(const SingleChoiceQuestion& other) {
	logger = other.logger;
	correctAnswer = other.correctAnswer;
	ChoiceQuestion::operator=(other);
	return *this;
}

SingleChoiceQuestion& SingleChoiceQuestion::operator=(SingleChoiceQuestion&& other) {
	logger = std::move(other.logger);
	correctAnswer = other.correctAnswer;
	other.correctAnswer = UINT8_MAX;
	ChoiceQuestion::operator=(std::forward<SingleChoiceQuestion&&>(other));
	return *this;
}

std::istream& operator>>(std::istream& input, SingleChoiceQuestion& inputQuestion) {
	inputQuestion.read(input);
	return input;
}


const uint8_t SingleChoiceQuestion::getCorrectAnswer() const noexcept {
	return correctAnswer;
}

float SingleChoiceQuestion::checkAnswer(const std::string& userAnswer) const {
	if (userAnswer.empty()) {
		return 0;
	}
	return correctAnswer == std::stoi(userAnswer);
}

std::shared_ptr<Question> SingleChoiceQuestion::copy() const {
	return std::make_shared<SingleChoiceQuestion>(SingleChoiceQuestion(*this));
}