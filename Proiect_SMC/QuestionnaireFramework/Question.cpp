#include <iostream>

#include "Question.h"

void Question::read(std::istream& input) {
	std::getline(input, questionText);
}

void Question::write(std::ostream& output) const {
	output << questionText << std::endl;
}

Question::Question(const std::string& questionText)
	:questionText(questionText) {}

Question::Question(const Question& other)
	: questionText(other.questionText) {}

Question::Question(Question&& other) {
	*this = std::forward<Question&&>(other);
}

Question& Question::operator=(const Question& other) {
	questionText = other.questionText;
	return *this;
}

Question& Question::operator=(Question&& other) {
	questionText = std::move(other.questionText);
	return *this;
}

std::istream& operator>>(std::istream& input, Question& question) {
	question.read(input);
	return input;
}

std::ostream& operator<<(std::ostream& output, const Question& question) {
	question.write(output);
	return output;
}

const std::string& Question::getQuestionText() const noexcept {
	return questionText;
}
