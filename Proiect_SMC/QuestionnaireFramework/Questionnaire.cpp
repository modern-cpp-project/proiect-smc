#include <iostream>
#include <sstream>

#include "Questionnaire.h"
#include "SingleChoiceQuestion.h"
#include "MultipleChoiceQuestion.h"
#include "InputQuestion.h"

const std::string Questionnaire::INDEX_OUT_OF_BOUNDS_ERROR = "Index out of bounds";

Questionnaire::Questionnaire(std::shared_ptr<Logger> logger)
	: logger(logger) {}

Questionnaire::Questionnaire(const std::vector<std::shared_ptr<Question>>&& questions, std::shared_ptr<Logger> logger)
	: questions(questions), answers(questions.size(), std::string()), flags(questions.size(), false), logger(logger) {}

Questionnaire::Questionnaire(const Questionnaire& other)
	: questions(other.questions), answers(other.answers), flags(other.flags), logger(other.logger) {}

Questionnaire::Questionnaire(Questionnaire&& other) {
	*this = std::forward<Questionnaire&&>(other);
}

Questionnaire Questionnaire::operator=(const Questionnaire& other) {
	questions = other.questions;
	answers = other.answers;
	flags = other.flags;
	logger = other.logger;
	return *this;
}

Questionnaire Questionnaire::operator=(Questionnaire&& other) {
	questions = std::move(other.questions);
	answers = std::move(other.answers);
	flags = std::move(other.flags);
	logger = other.logger;
	return *this;
}

void Questionnaire::updateAnswer(uint16_t index, const std::string& answer) {
	if (index < questions.size()) {
		answers[index] = answer;
	}
}

std::string Questionnaire::getAnswer(uint16_t index) const {
	if (index < questions.size()) {
		return answers[index];
	}
	outOfBoundsLogger(index);
	throw INDEX_OUT_OF_BOUNDS_ERROR;
}

void Questionnaire::updateFlag(uint16_t index, bool flag) {
	if (index < questions.size()) {
		flags[index] = flag;
	}
}

const bool Questionnaire::getFlag(uint16_t index) const {
	if (index < questions.size()) {
		return flags[index];
	}
	outOfBoundsLogger(index);
	throw INDEX_OUT_OF_BOUNDS_ERROR;
}

const std::shared_ptr<Question>& Questionnaire::getQuestion(uint16_t index) const {
	return questions[index];
}

const uint16_t Questionnaire::getSize() const {
	return questions.size();
}

float Questionnaire::calculateGrade() const {
	float grade = 0;
	for (uint8_t index = 0; index < questions.size(); ++index) {
		float temp = questions[index]->checkAnswer(answers[index]);
		grade += temp;
	}
	return grade / questions.size() * 9 + 1;
}

void Questionnaire::outOfBoundsLogger(uint16_t index) const {
	logger->log(INDEX_OUT_OF_BOUNDS_ERROR + " " + std::to_string(index), Logger::Level::ERROR);
}
