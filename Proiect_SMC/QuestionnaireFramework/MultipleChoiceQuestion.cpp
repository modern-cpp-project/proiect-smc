#include <sstream>
#include <iostream>
#include <regex>

#include "MultipleChoiceQuestion.h"

const std::string MultipleChoiceQuestion::REGEX_CORRECT_ANSWERS = R"((0|1|2|3)( (0|1|2|3)){0,3})";

void MultipleChoiceQuestion::read(std::istream& input) {
	ChoiceQuestion::read(input);
	std::string correctAnswersLine;
	std::getline(input, correctAnswersLine);
	std::istringstream stream(correctAnswersLine);
	uint16_t correctAnswerInput;
	std::regex correctAnswersFormat{ REGEX_CORRECT_ANSWERS };
	if (!std::regex_match(correctAnswersLine, correctAnswersFormat)) {
		logger->log(MultipleChoiceQuestion::CORRECT_ANSWER_INVALID, Logger::Level::ERROR);
		throw MultipleChoiceQuestion::CORRECT_ANSWER_INVALID;
	}
	while (stream >> correctAnswerInput) {
		correctAnswers[static_cast<uint8_t>(correctAnswerInput)] = true;
	}
}

void MultipleChoiceQuestion::write(std::ostream& output) const {
	ChoiceQuestion::write(output);
	std::string correctAnswerString = "";
	for (uint8_t index = 0; index < correctAnswers.size(); ++index) {
		if (correctAnswers[index]) {
			correctAnswerString += std::to_string(unsigned(index)) + ' ';
		}
	}
	correctAnswerString.pop_back();
	output << correctAnswerString << std::endl;
}

MultipleChoiceQuestion::MultipleChoiceQuestion(std::shared_ptr<Logger> logger)
	: correctAnswers(std::array<bool, MultipleChoiceQuestion::ANSWERS_NUMBER>{false, false, false, false}), logger(logger) {}

MultipleChoiceQuestion::MultipleChoiceQuestion(const std::string& questionText, const std::array<std::string, ANSWERS_NUMBER>& answers, const std::array<bool, ANSWERS_NUMBER>& correctAnswers, std::shared_ptr<Logger> logger)
	: ChoiceQuestion(questionText, answers), correctAnswers(correctAnswers), logger(logger) {}

MultipleChoiceQuestion::MultipleChoiceQuestion(const MultipleChoiceQuestion& other)
	: ChoiceQuestion(other.questionText, other.answers), correctAnswers(other.correctAnswers), logger(other.logger) {}

MultipleChoiceQuestion::MultipleChoiceQuestion(MultipleChoiceQuestion&& other) {
	*this = std::forward<MultipleChoiceQuestion&&>(other);
}

MultipleChoiceQuestion& MultipleChoiceQuestion::operator=(const MultipleChoiceQuestion& other) {
	logger = other.logger;
	correctAnswers = other.correctAnswers;
	ChoiceQuestion::operator=(other);
	return *this;
}

MultipleChoiceQuestion& MultipleChoiceQuestion::operator=(MultipleChoiceQuestion&& other) {
	logger = std::move(other.logger);
	correctAnswers = std::move(other.correctAnswers);
	ChoiceQuestion::operator=(std::forward<MultipleChoiceQuestion&&>(other));
	return *this;
}

std::istream& operator>>(std::istream& input, MultipleChoiceQuestion& inputQuestion) {
	inputQuestion.read(input);
	return input;
}

const std::array<bool, ChoiceQuestion::ANSWERS_NUMBER>& MultipleChoiceQuestion::getCorrectAnswers() const noexcept {
	return correctAnswers;
}

float MultipleChoiceQuestion::checkAnswer(const std::string& userAnswer) const {
	if (userAnswer.empty()) {
		return 0;
	}
	uint8_t correctAnswersNumber = 0;
	uint8_t userCorrectAnswersNumber = 0;
	for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
		if (correctAnswers[index]) {
			++correctAnswersNumber;
		}
		if (userAnswer[index] == '0') {
			continue;
		}
		if (!correctAnswers[index]) {
			return 0;
		}
		++userCorrectAnswersNumber;
	}
	if (correctAnswersNumber == 0) {
		return 0;
	}
	return userCorrectAnswersNumber * 1.0 / correctAnswersNumber;
}

std::shared_ptr<Question> MultipleChoiceQuestion::copy() const {
	return std::make_shared<MultipleChoiceQuestion>(MultipleChoiceQuestion(*this));
}