#pragma once

#include <string>
#include <memory>

class __declspec(dllexport) Question {
protected:
	std::string questionText;

	Question() = default;
	virtual void read(std::istream& input);
	virtual void write(std::ostream& output) const;

public:
	Question(const std::string& questionText);
	virtual ~Question() = default;
	Question(const Question& other);
	Question(Question&& other);
	Question& operator=(const Question& other);
	Question& operator=(Question&& other);
	friend std::istream& operator>>(std::istream& input, Question& question);
	friend std::ostream& operator<<(std::ostream& output, const Question& question);
	const std::string& getQuestionText() const noexcept;
	virtual float checkAnswer(const std::string& userAnswer) const = 0;
	virtual std::shared_ptr<Question> copy() const = 0;
};

