#include <random>

template<class T>
void shuffleVector(std::vector<T>& vec) {
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::shuffle(vec.begin(), vec.end(), generator);
}