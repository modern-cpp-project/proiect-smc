#pragma once

#include "../Logger/Logger.h"

#include "Question.h"

class __declspec(dllexport) InputQuestion : public Question {
private:
	std::string correctAnswer;
	std::shared_ptr<Logger> logger;

protected:
	void read(std::istream& input);
	void write(std::ostream& output) const;
public:
	InputQuestion() = default;
	InputQuestion(const std::string& questionText, const std::string& correctAnswer);
	~InputQuestion() = default;
	InputQuestion(const InputQuestion& other);
	InputQuestion(InputQuestion&& other);
	InputQuestion& operator=(const InputQuestion& other);
	InputQuestion& operator=(InputQuestion&& other);
	friend std::istream& operator>>(std::istream& input, InputQuestion& inputQuestion);
	void setCorrectAnswer(const std::string& correctAnswer);
	const std::string& getCorrectAnswer() const;
	float checkAnswer(const std::string& userAnswer) const;
	std::shared_ptr<Question> copy() const;
};