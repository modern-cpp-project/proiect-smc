#include <iostream>

#include "InputQuestion.h"

void InputQuestion::read(std::istream& input)
{
	Question::read(input);
	std::getline(input, correctAnswer);
	return;
}

void InputQuestion::write(std::ostream& output) const {
	Question::write(output);
	output << correctAnswer << std::endl;
}

InputQuestion::InputQuestion(const std::string& questionText, const std::string& correctAnswer)
	:Question(questionText), correctAnswer(correctAnswer) {}

InputQuestion::InputQuestion(const InputQuestion& other)
	: Question(other.questionText), correctAnswer(other.correctAnswer) {}

InputQuestion::InputQuestion(InputQuestion&& other) {
	*this = std::forward<InputQuestion&&>(other);
}

InputQuestion& InputQuestion::operator=(const InputQuestion& other) {
	correctAnswer = other.correctAnswer;
	Question::operator=(other);
	return *this;
}

InputQuestion& InputQuestion::operator=(InputQuestion&& other) {
	correctAnswer = std::move(other.correctAnswer);
	Question::operator=(std::forward<InputQuestion&&>(other));
	return *this;
}

std::istream& operator>>(std::istream& input, InputQuestion& inputQuestion) {
	inputQuestion.read(input);
	return input;
}

void InputQuestion::setCorrectAnswer(const std::string& correctAnswer) {
	this->correctAnswer = correctAnswer;
}

const std::string& InputQuestion::getCorrectAnswer() const {
	return correctAnswer;
}

float InputQuestion::checkAnswer(const std::string& userAnswer) const {
	return correctAnswer == userAnswer;
}

std::shared_ptr<Question> InputQuestion::copy() const {
	return std::make_shared<InputQuestion>(InputQuestion(*this));
}
