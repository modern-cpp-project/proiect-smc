#include <iostream>

#include "ChoiceQuestion.h"

const std::string ChoiceQuestion::CORRECT_ANSWER_INVALID = "Correct answer is invalid";
const std::string ChoiceQuestion::DUPLICATED_ANSWERS_ERROR = "Duplicated answers";

void ChoiceQuestion::read(std::istream& input) {
	Question::read(input);
	for (uint8_t index = 0; index < ANSWERS_NUMBER; ++index) {
		std::getline(input, answers[index]);
		if (std::find(answers.begin(), answers.begin() + index, answers[index]) != answers.begin() + index) {
			throw DUPLICATED_ANSWERS_ERROR;
		}
	}
}

void ChoiceQuestion::write(std::ostream& output) const {
	Question::write(output);
	for (uint8_t index = 0; index < ANSWERS_NUMBER; ++index) {
		output << answers[index] << std::endl;
	}
}

ChoiceQuestion::ChoiceQuestion(const std::string& questionText, const std::array<std::string, ANSWERS_NUMBER>& answers)
	:Question(questionText), answers(answers) {}

ChoiceQuestion::ChoiceQuestion(const ChoiceQuestion& other)
	: Question(other.questionText), answers(other.answers) {}

ChoiceQuestion::ChoiceQuestion(ChoiceQuestion&& other) {
	*this = std::forward<ChoiceQuestion&&>(other);
}

ChoiceQuestion& ChoiceQuestion::operator=(const ChoiceQuestion& other) {
	answers = other.answers;
	Question::operator=(other);
	return *this;
}

ChoiceQuestion& ChoiceQuestion::operator=(ChoiceQuestion&& other) {
	answers = std::move(other.answers);
	Question::operator=(std::forward<ChoiceQuestion&&>(other));
	return *this;
}

const std::string& ChoiceQuestion::getQuestionAnswer(uint8_t index) const noexcept {
	return answers[index];
}
