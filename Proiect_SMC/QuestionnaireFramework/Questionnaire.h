#pragma once

#include <vector>

#include "../Logger/Logger.h"

#include "Question.h"

class __declspec(dllexport) Questionnaire {
private:
	std::vector<std::shared_ptr<Question>> questions;
	static const std::string INDEX_OUT_OF_BOUNDS_ERROR;
	std::vector<std::string> answers;
	std::vector<bool> flags;
	std::shared_ptr<Logger> logger;

public:
	Questionnaire() = default;
	Questionnaire(std::shared_ptr<Logger> logger);
	Questionnaire(const std::vector<std::shared_ptr<Question>>&& questions,std::shared_ptr<Logger> logger);
	~Questionnaire() = default;
	Questionnaire(const Questionnaire& other);
	Questionnaire(Questionnaire&& other);
	Questionnaire operator=(const Questionnaire& other);
	Questionnaire operator=(Questionnaire&& other);
	void updateAnswer(uint16_t index, const std::string& answer);
	std::string getAnswer(uint16_t index) const;
	void updateFlag(uint16_t index, bool flag);
	const bool getFlag(uint16_t index) const;
	const std::shared_ptr<Question>& getQuestion(uint16_t index) const;
	const uint16_t getSize() const;
	float calculateGrade() const;
	void outOfBoundsLogger(uint16_t index) const;
};