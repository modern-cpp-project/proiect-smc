﻿#include <fstream>
#include <algorithm>

#include "QuestionBank.h"

const std::string QuestionBank::LOADED_QUESTIONS = "Successfully loaded questions!";

QuestionBank::QuestionBank(std::istream& ist, std::shared_ptr<Logger> logger)
	: logger(logger) {
	while (!ist.eof()) {
		std::string chapterName;
		std::getline(ist, chapterName);
		if (chapterName.empty()) {
			continue;
		}
		Chapter chapter(chapterName, logger);
		ist >> chapter;
		questionsNumber += chapter.getQuestionsNumber();
		chapters.push_back(std::move(chapter));
	}
	logger->log(LOADED_QUESTIONS, Logger::Level::INFO);
}

QuestionBank::QuestionBank(const QuestionBank& other)
	: chapters(other.chapters), questionsNumber(other.questionsNumber), logger(other.logger) {}

QuestionBank::QuestionBank(QuestionBank&& other) {
	*this = std::forward<QuestionBank&&>(other);
}

QuestionBank& QuestionBank::operator=(const QuestionBank& other) {
	logger = other.logger;
	chapters = other.chapters;
	questionsNumber = other.questionsNumber;
	return *this;
}

QuestionBank& QuestionBank::operator=(QuestionBank&& other) {
	logger = std::move(other.logger);
	chapters = std::move(other.chapters);
	questionsNumber = std::move(other.questionsNumber);
	return *this;
}

Questionnaire QuestionBank::createQuestionnaire(const uint16_t questionnaireSize) {
	uint16_t questionsNumber = questionnaireSize / chapters.size();
	uint16_t questionsPerChapter = questionsNumber == 0 ? 1 : questionsNumber;
	uint16_t indexChapter = 0;
	std::vector<std::shared_ptr<Question>> questions;
	bool shuffle = true;
	bool remainingQuestionsChapters = true;
	while (questions.size() < questionnaireSize && remainingQuestionsChapters) {
		remainingQuestionsChapters = false;
		for (auto& chapter : chapters) {
			std::vector<std::shared_ptr<Question>> questionsChapter = chapter.shuffleAndGetQuestions(indexChapter, questionsPerChapter, shuffle);
			if (!questionsChapter.empty()) {
				remainingQuestionsChapters = true;
				questions.insert(questions.end(), questionsChapter.begin(), questionsChapter.end());
			}
			if (questions.size() == questionnaireSize) {
				break;
			}
		}
		indexChapter += questionsPerChapter;
		uint16_t remainingQuestions = (questionnaireSize - questions.size()) / chapters.size();
		questionsPerChapter = remainingQuestions == 0 ? 1 : remainingQuestions;
		shuffle = false;
	}
	return Questionnaire(std::move(questions), logger);
}

const uint32_t QuestionBank::getQuestionsNumber() const {
	return questionsNumber;
}

const uint16_t QuestionBank::getChaptersNumber() const {
	return chapters.size();
}

Chapter& QuestionBank::getChapter(const uint16_t index) {
	return chapters[index];
}

const Chapter& QuestionBank::getChapter(const uint16_t index) const {
	return chapters[index];
}

const std::string QuestionBank::getFormattedChapter(const uint16_t index) const {
	return chapters[index].getChapterName() + " - " + std::to_string(chapters[index].getQuestionsNumber());
}

void QuestionBank::addChapter(const std::string& chapterName) {
	chapters.emplace_back(chapterName, logger);
}

void QuestionBank::deleteChapter(const int index) {
	auto chapterIterator = chapters.begin();
	std::advance(chapterIterator, index);
	questionsNumber -= chapterIterator->getQuestionsNumber();
	chapters.erase(chapterIterator);
}

void QuestionBank::saveQuestions(std::ostream& ost) const {
	std::for_each(chapters.begin(), chapters.end(), [&](const Chapter& chapter) {
		ost << chapter;
		});
}

void QuestionBank::incrementDecrement(bool increment) {
	increment ? ++questionsNumber : --questionsNumber;
}
