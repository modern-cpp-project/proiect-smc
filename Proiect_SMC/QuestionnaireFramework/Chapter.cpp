#include <iostream>
#include <algorithm>

#include "Chapter.h"
#include "SingleChoiceQuestion.h"
#include "MultipleChoiceQuestion.h"
#include "InputQuestion.h"
#include "Utils.cpp"

const std::string Chapter::SINGLE_CHOICE_QUESTION = "SingleChoice";
const std::string Chapter::MULTIPLE_CHOICE_QUESTION = "MultipleChoice";
const std::string Chapter::INPUT_QUESTION = "Input";
const std::string Chapter::END_OF_CHAPTER = "End of Chapter";
const std::string Chapter::FACTORY_OUT_OF_BOUNDS = "Factory out of bounds";
const std::map<std::string, std::function<std::shared_ptr<Question>(uint8_t& lines, std::shared_ptr<Logger>& logger)>> Chapter::questionFactory = {
		{"SingleChoice", [](uint8_t& lines, std::shared_ptr<Logger>& logger) -> std::shared_ptr<Question> { return std::shared_ptr<Question>(new SingleChoiceQuestion(logger)); }},
		{"MultipleChoice", [](uint8_t& lines, std::shared_ptr<Logger>& logger) -> std::shared_ptr<Question> { return std::shared_ptr<Question>(new MultipleChoiceQuestion(logger)); }},
		{"Input", [](uint8_t& lines, std::shared_ptr<Logger>& logger) -> std::shared_ptr<Question> { lines = Chapter::INPUT_LINES; return std::shared_ptr<Question>(new InputQuestion()); }}
};

Chapter::Chapter(const std::string& chapterName, std::shared_ptr<Logger> logger)
	: chapterName(chapterName), logger(logger) {}

Chapter::Chapter(const Chapter& other)
	: logger(other.logger), chapterName(other.chapterName) {
	questions.reserve(other.questions.size());
	copyQuestions(other);
}

Chapter::Chapter(Chapter&& other) {
	*this = std::forward<Chapter&&>(other);
}

Chapter& Chapter::operator=(const Chapter& other) {
	logger = other.logger;
	chapterName = other.chapterName;
	questions.clear();
	questions.reserve(other.questions.size());
	copyQuestions(other);
	return *this;
}

Chapter& Chapter::operator=(Chapter&& other) {
	logger = std::move(other.logger);
	chapterName = std::move(other.chapterName);
	questions = std::move(other.questions);
	return *this;
}

std::istream& operator>>(std::istream& input, Chapter& chapter) {
	uint8_t lines = 0;
	if (input.eof()) {
		return input;
	}
	while (!input.eof()) {
		std::string questionType;
		std::getline(input, questionType);
		std::shared_ptr<Question> question(nullptr);
		lines = Chapter::CHOICE_LINES;
		if (questionType == Chapter::END_OF_CHAPTER) {
			break;
		}
		else if (questionType.empty()) {
			continue;
		}
		try {
			question = Chapter::questionFactory.at(questionType)(lines, chapter.logger);
		}
		catch (std::out_of_range exp) {
			chapter.logger->log(Chapter::FACTORY_OUT_OF_BOUNDS, Logger::Level::ERROR);
		}
		if (question.get() != nullptr && chapter.checkQuestion(input, lines)) {
			try {
				input >> *question;
				chapter.questions.push_back(question);
			}
			catch (std::string error) {
				chapter.logger->log(error, Logger::Level::ERROR);
			}
		}
	}
	return input;
}

std::ostream& operator<<(std::ostream& output, const Chapter& chapter) {
	output << chapter.chapterName << std::endl;
	std::for_each(chapter.questions.begin(), chapter.questions.end(), [&](const std::shared_ptr<Question>& question) {
		if (std::dynamic_pointer_cast<SingleChoiceQuestion>(question)) {
			output << Chapter::SINGLE_CHOICE_QUESTION;
		}
		else if (std::dynamic_pointer_cast<MultipleChoiceQuestion>(question)) {
			output << Chapter::MULTIPLE_CHOICE_QUESTION;
		}
		else if (std::dynamic_pointer_cast<InputQuestion>(question)) {
			output << Chapter::INPUT_QUESTION;
		}
		output << std::endl << *question;
		});
	output << Chapter::END_OF_CHAPTER << std::endl;
	return output;
}

std::vector<std::shared_ptr<Question>> Chapter::shuffleAndGetQuestions(uint16_t index, uint16_t questionsNumber, bool shuffle) {
	if (shuffle) {
		shuffleVector(questions);
	}
	std::vector<std::shared_ptr<Question>> selectedQuestions;
	if (index < questions.size()) {
		questionsNumber = std::min(questionsNumber, static_cast<uint16_t>(questions.size() - index));
		std::copy(questions.begin() + index,
			questions.begin() + index + questionsNumber,
			std::back_inserter(selectedQuestions));
	}
	return selectedQuestions;
}

const uint16_t Chapter::getQuestionsNumber() const {
	return questions.size();
}

const std::string& Chapter::getChapterName() const {
	return chapterName;
}

void Chapter::setChapterName(const std::string& chapterName) {
	this->chapterName = chapterName;
}

const std::shared_ptr<Question>& Chapter::getQuestion(uint16_t index) const {
	return questions[index];
}

void Chapter::setQuestion(uint16_t index, const std::shared_ptr<Question>& question) {
	questions[index] = question;
}

void Chapter::addQuestion(const std::shared_ptr<Question>& question) {
	questions.push_back(question);
}

void Chapter::deleteQuestion(uint16_t index) {
	questions.erase(questions.begin() + index);
}

void Chapter::copyQuestions(const Chapter& other) {
	std::transform(other.questions.begin(), other.questions.end(), std::back_inserter(questions), [](auto& question) {
		return question->copy();
		});
}

bool Chapter::checkQuestion(std::istream& input, const uint8_t linesToSkip) {
	std::streampos currentPos = input.tellg();
	std::string line;
	bool questionValid = true;
	for (uint8_t index = 0; index < linesToSkip && !input.eof(); ++index) {
		std::getline(input, line);
		if (line == Chapter::SINGLE_CHOICE_QUESTION || line == Chapter::MULTIPLE_CHOICE_QUESTION || line == Chapter::INPUT_QUESTION || line == Chapter::END_OF_CHAPTER) {
			questionValid = false;
		}
	}
	input.seekg(currentPos);
	return questionValid;
}


