#pragma once

#include "../Logger/Logger.h"

#include "ChoiceQuestion.h"

class __declspec(dllexport) MultipleChoiceQuestion : public ChoiceQuestion {
private:
	std::array<bool, ANSWERS_NUMBER> correctAnswers;
	static const std::string REGEX_CORRECT_ANSWERS;
	std::shared_ptr<Logger> logger;

protected:
	void read(std::istream& input);
	void write(std::ostream& output) const;
public:
	MultipleChoiceQuestion(std::shared_ptr<Logger> logger);
	MultipleChoiceQuestion(const std::string& questionText, const std::array<std::string, ANSWERS_NUMBER>& answers, const std::array<bool, ANSWERS_NUMBER>& correctAnswers, std::shared_ptr<Logger> logger);
	~MultipleChoiceQuestion() = default;
	MultipleChoiceQuestion(const MultipleChoiceQuestion& other);
	MultipleChoiceQuestion(MultipleChoiceQuestion&& other);
	MultipleChoiceQuestion& operator=(const MultipleChoiceQuestion& other);
	MultipleChoiceQuestion& operator=(MultipleChoiceQuestion&& other);
	friend std::istream& operator>>(std::istream& input, MultipleChoiceQuestion& inputQuestion);
	const std::array<bool, ANSWERS_NUMBER>& getCorrectAnswers() const noexcept;
	float checkAnswer(const std::string& userAnswer) const;
	std::shared_ptr<Question> copy() const;
};

