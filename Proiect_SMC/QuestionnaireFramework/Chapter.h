#pragma once

#include <vector>
#include <map>
#include <functional>

#include "../Logger/Logger.h"

#include "Question.h"

class __declspec(dllexport) Chapter {
private:
	std::vector<std::shared_ptr<Question>> questions;
	std::shared_ptr<Logger> logger;
	std::string chapterName;

	static const std::string SINGLE_CHOICE_QUESTION;
	static const std::string MULTIPLE_CHOICE_QUESTION;
	static const std::string INPUT_QUESTION;
	static const std::string END_OF_CHAPTER;
	static const std::string FACTORY_OUT_OF_BOUNDS;
	static const uint8_t CHOICE_LINES = 6;
	static const uint8_t INPUT_LINES = 2;

	static const std::map<std::string, std::function<std::shared_ptr<Question>(uint8_t& lines, std::shared_ptr<Logger>& logger)>> questionFactory;

public:
	Chapter(const std::string& chapterName, std::shared_ptr<Logger> logger);
	Chapter(const Chapter& other);
	Chapter(Chapter&& other);
	Chapter& operator=(const Chapter& other);
	Chapter& operator=(Chapter&& other);
	friend std::istream& operator>>(std::istream& input, Chapter& chapter);
	friend std::ostream& operator<<(std::ostream& output, const Chapter& chapter);
	std::vector<std::shared_ptr<Question>> shuffleAndGetQuestions(uint16_t index, uint16_t questionsNumber, bool shuffle);
	const uint16_t getQuestionsNumber() const;
	const std::string& getChapterName() const;
	void setChapterName(const std::string& chapterName);
	const std::shared_ptr<Question>& getQuestion(uint16_t index) const;
	void setQuestion(uint16_t index, const std::shared_ptr<Question>& question);
	void addQuestion(const std::shared_ptr<Question>& question);
	void deleteQuestion(uint16_t index);

private:
	void copyQuestions(const Chapter& other);
	bool checkQuestion(std::istream& input, const uint8_t linesToSkip);

};