#include "CustomRadioButton.h"

CustomRadioButton::CustomRadioButton(QWidget* parent)
	:QRadioButton(parent), wordWrapWidget(new WordWrapWidget(this)) {
    init();
}

void CustomRadioButton::setText(const QString& text) {
    wordWrapWidget->getLabel()->setText(text);
}

QSize CustomRadioButton::sizeHint() const {
    return wordWrapWidget->sizeHint();
}

void CustomRadioButton::labelIsClicked() {
    setChecked(!isChecked());
}

void CustomRadioButton::resizeEvent(QResizeEvent* event) {
    QWidget::resizeEvent(event);
    updateGeometry();
}

void CustomRadioButton::init() {
    connect(wordWrapWidget->getLabel().get(), SIGNAL(clicked()), this, SLOT(labelIsClicked()));
}
