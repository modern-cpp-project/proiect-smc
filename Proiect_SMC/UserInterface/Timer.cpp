#include "Timer.h"

Timer::Timer()
	:running(false) {}

const std::chrono::time_point<std::chrono::system_clock>& Timer::getStart() const {
	return start;
}

const std::chrono::time_point<std::chrono::system_clock>& Timer::getEnd() const {
	return end;
}

void Timer::Start() {
	start = std::chrono::system_clock::now();
	running = true;
}

void Timer::Stop() {
	end = std::chrono::system_clock::now();
	running = false;
}

const uint32_t Timer::getTimeDelta() const {
	std::chrono::time_point<std::chrono::system_clock> endTime;
	if (running) {
		endTime = std::chrono::system_clock::now();
	}
	else {
		endTime = end;
	}
	return std::chrono::duration_cast<std::chrono::seconds>(endTime - start).count();
}