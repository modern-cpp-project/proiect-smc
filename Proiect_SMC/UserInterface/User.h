#pragma once

#include <string>
#include <ostream>

class User {
private:
	std::string name;

public:
	User() = default;
	User(const std::string& name);
	User(const User& other);
	User(User&& other);
	User& operator=(const User& other);
	User& operator=(User&& other);
	friend std::ostream& operator<<(std::ostream& output, const User& user);
	const std::string& getName() const noexcept;
};

