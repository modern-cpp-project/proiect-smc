#pragma once

#include <string>
#include <QString>
#include <QColor>

class K {
	friend class UserInterface;
	friend class WordWrapWidget;

	static const std::string PROGRAM_NAME;
	static const std::string START_QUESTIONNAIRE_TIME;
	static const std::string FINISH_QUESTIONNAIRE_TIME;
	static const std::string USER_NAME;
	static const std::string USER_GRADE;
	static const std::string USER_ANSWERS;
	static const QString CANNOT_OPEN_FILE;
	static const QString NO_CHAPTERS;
	static const QString NO_QUESTIONS;
	static const QString SINGLE_CHOICE;
	static const QString MULTIPLE_CHOICE;
	static const QString ADD_FLAG;
	static const QString REMOVE_FLAG;
	static const QString RESULT_TEXT;
	static const QString NO_ANSWER;
	static const uint8_t MINIMUM_NAME_SIZE = 5;
	static const uint16_t QUESTIONNAIRE_SIZE = UINT16_MAX;
	static const uint8_t SINGLE_CHOICE_INDEX_PAGE = 0;
	static const uint8_t MULTIPLE_CHOICE_INDEX_PAGE = 1;
	static const uint8_t INPUT_INDEX_PAGE = 2;
	static const uint8_t MAXIMUM_QUESTION_LENGTH = 80;
	static const uint8_t MAXIMUM_COLUMNS_QUESTION_TABLE = 5;
	static const uint16_t DEFAULT_TIMER_MINUTES = 10;
	static const QColor ANSWER_BACKGROUND_COLOR;
	static const QColor NO_ANSWER_BACKGROUND_COLOR;
	static const QColor ANSWERED_BACKGROUND_COLOR;
	static const QColor BASE_TABLE_COLOR;
	static const QColor BASE_COLOR;
	static const QColor FLAG_COLOR;
	static const uint8_t PADDING_CUSTOM_WIDGET = 30;
	static const quint64 ENCRYPTION_KEY = Q_UINT64_C(0x0c5bc8a5eba9f852);

	class LOG {
	public:
		static const std::string FILE;
		static const std::string PRESSED;
		static const QString START_BUTTON;
		static const QString PREVIOUS_QUESTION;
		static const QString NEXT_QUESTION;
		static const QString FINISH_QUESTIONNAIRE;
		static const QString RESTART_QUESTIONNAIRE;
		static const QString SHOW_GRADE;
		static const QString LIST_ITEM;
		static const QString TABLE_ITEM;
		static const QString RESET_ANSWER;
		static const QString ADD_FLAG_QUESTION;
		static const QString REMOVE_FLAG_QUESTION;
	};
};

