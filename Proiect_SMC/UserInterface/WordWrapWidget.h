#pragma once

#include <QAbstractButton>
#include <QHBoxLayout>

#include "ClickableLabel.h"

class WordWrapWidget {
private:
	QAbstractButton* button;
	std::unique_ptr<QHBoxLayout> layout;
	std::unique_ptr<ClickableLabel> label;

public:
	WordWrapWidget(QAbstractButton* button);
	QSize sizeHint() const;
	const std::unique_ptr<ClickableLabel>& getLabel() const;

public slots:
	void labelIsClicked();

private:
	void init();
};

