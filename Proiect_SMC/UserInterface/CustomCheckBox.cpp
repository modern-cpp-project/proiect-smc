#include "CustomCheckBox.h"

CustomCheckBox::CustomCheckBox(QWidget* parent)
    :QCheckBox(parent), wordWrapWidget(new WordWrapWidget(this)) {
    init();
}

void CustomCheckBox::setText(const QString& text) {
    wordWrapWidget->getLabel()->setText(text);
}

QSize CustomCheckBox::sizeHint() const {
    return wordWrapWidget->sizeHint();
}

void CustomCheckBox::labelIsClicked() {
    setChecked(!isChecked());
}

void CustomCheckBox::resizeEvent(QResizeEvent* event) {
    QWidget::resizeEvent(event);
    updateGeometry();
}

void CustomCheckBox::init() {
    connect(wordWrapWidget->getLabel().get(), SIGNAL(clicked()), this, SLOT(labelIsClicked()));
}
