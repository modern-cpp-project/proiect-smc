#pragma once

#include <QObject>
#include <QLabel>

class ClickableLabel : public QLabel {
private:
	Q_OBJECT;

public:
	explicit ClickableLabel(QWidget* parent = Q_NULLPTR);
	~ClickableLabel();

signals:
	void clicked();

protected:
	void mousePressEvent(QMouseEvent* event);
};

