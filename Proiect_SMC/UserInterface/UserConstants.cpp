﻿#include "UserConstants.h"

const std::string K::PROGRAM_NAME = "Questionnaire";
const std::string K::START_QUESTIONNAIRE_TIME = "Data inceperii chestionarului: ";
const std::string K::FINISH_QUESTIONNAIRE_TIME = "Data terminarii chestionarului: ";
const std::string K::USER_NAME = "Numele: ";
const std::string K::USER_GRADE = "Nota: ";
const std::string K::USER_ANSWERS = "Raspunsurile utilizatorului pentru intrebari: ";
const QString K::CANNOT_OPEN_FILE = QString::fromWCharArray(L"Nu se poate deschide fișierul cu întrebări. Vă rugăm încercați din nou.");
const QString K::NO_CHAPTERS = QString::fromWCharArray(L"Fișierul nu conține niciun capitol!");
const QString K::NO_QUESTIONS = QString::fromWCharArray(L"Fișierul nu conține nicio intrebare!");
const QString K::SINGLE_CHOICE = "singleChoice";
const QString K::MULTIPLE_CHOICE = "multipleChoice";
const QString K::ADD_FLAG = QString::fromWCharArray(L"Adaugă flag");
const QString K::REMOVE_FLAG = QString::fromWCharArray(L"Șterge flag");
const QString K::RESULT_TEXT = QString::fromWCharArray(L"Rezultatul dumneavoastră este: ");
const QString K::NO_ANSWER = QString::fromWCharArray(L" - niciun răspuns");
const QColor K::ANSWER_BACKGROUND_COLOR = QColor(65, 65, 65);
const QColor K::NO_ANSWER_BACKGROUND_COLOR = QColor(140, 40, 40);
const QColor K::ANSWERED_BACKGROUND_COLOR = QColor(130, 130, 180);
const QColor K::BASE_TABLE_COLOR = QColor(83, 83, 83);
const QColor K::BASE_COLOR = QColor(255, 255, 255);
const QColor K::FLAG_COLOR = QColor(255, 210, 40);

const std::string K::LOG::FILE = "../output/UserInterfaceLog.txt";
const std::string K::LOG::PRESSED = " PRESSED";
const QString K::LOG::START_BUTTON = "startButton";
const QString K::LOG::PREVIOUS_QUESTION = "previousQuestion";
const QString K::LOG::NEXT_QUESTION = "nextQuestion";
const QString K::LOG::FINISH_QUESTIONNAIRE = "finishQuestionnaire";
const QString K::LOG::RESTART_QUESTIONNAIRE = "restart";
const QString K::LOG::SHOW_GRADE = "showGrade";
const QString K::LOG::LIST_ITEM = "List item ";
const QString K::LOG::TABLE_ITEM = "Table item ";
const QString K::LOG::RESET_ANSWER = "Reset answer ";
const QString K::LOG::ADD_FLAG_QUESTION = "Add question flag ";
const QString K::LOG::REMOVE_FLAG_QUESTION = "Remove question flag ";