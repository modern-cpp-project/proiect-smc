#include <QStyleOptionButton>

#include "WordWrapWidget.h"
#include "UserConstants.h"

WordWrapWidget::WordWrapWidget(QAbstractButton* button): button(button), layout(new QHBoxLayout(button)), label(new ClickableLabel(button)){
	init();
}

void WordWrapWidget::labelIsClicked() {
    button->setChecked(!button->isChecked());
}

QSize WordWrapWidget::sizeHint() const {
    QFontMetrics fm(label->font());
    QRect r = label->rect();
    r.setLeft(r.left() + label->indent() + K::PADDING_CUSTOM_WIDGET);
    QRect bRect = fm.boundingRect(r, int(Qt::AlignLeft | Qt::AlignVCenter | Qt::TextWordWrap), label->text());
    QSize ret = QSize(button->QWidget::sizeHint().width(), bRect.height());
    return ret;
}

const std::unique_ptr<ClickableLabel>& WordWrapWidget::getLabel() const {
    return label;
}

void WordWrapWidget::init() {
    button->setLayout(layout.get());
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(label.get());
    label->setIndent(K::PADDING_CUSTOM_WIDGET);
    label->setWordWrap(true);
    button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
}


