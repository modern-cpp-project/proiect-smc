#pragma once

#include <QRadioButton>
#include <QHBoxLayout>

#include "WordWrapWidget.h"

class CustomRadioButton : public QRadioButton {
private:
	Q_OBJECT;
    std::unique_ptr<WordWrapWidget> wordWrapWidget;
public:
	CustomRadioButton(QWidget* parent = Q_NULLPTR);
    void setText(const QString& text);
    QSize sizeHint() const override;

private slots:
    void labelIsClicked();

protected:
    void resizeEvent(QResizeEvent* event) override;

private:
    void init();

};

