﻿#include <thread>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <QLabel>
#include <QDesktopWidget>
#include <QVBoxLayout>
#include <QString>
#include <QPushButton>
#include <QKeyEvent>

#include "UserInterface.h"

#include "../QuestionnaireFramework/SingleChoiceQuestion.h"
#include "../QuestionnaireFramework/MultipleChoiceQuestion.h"
#include "../QuestionnaireFramework/InputQuestion.h"
#include "../SimpleCrypt/SimpleCrypt.h"

UserInterface::UserInterface(const std::string& questionsFile, const std::string& programName,
	uint16_t questionnaireSize, bool showTimer, uint16_t minutes, QWidget* parent)
	:loggerStream(std::ofstream(K::LOG::FILE)),
	logger(std::make_shared<Logger>(loggerStream)),
	showTimer(showTimer),
	minutes(minutes),
	QMainWindow(parent) {
	questionnaire = Questionnaire(logger);
	init(programName);
	std::thread([this, questionsFile, questionnaireSize]() {
		try {
			std::ifstream ist(questionsFile);
			if (!ist.is_open()) {
				throw K::CANNOT_OPEN_FILE;
			}
			std::stringstream ss;
			std::string line;
			SimpleCrypt crypto(K::ENCRYPTION_KEY);
			while (std::getline(ist, line)) {
				ss << crypto.decryptToString(QString::fromStdString(line)).toStdString() << std::endl;
			}
			ist.close();
			ss.seekg(std::ios_base::beg);
			questionBank = QuestionBank(ss, logger);
			this->questionnaireSize = questionBank.getQuestionsNumber() < questionnaireSize ?
				questionBank.getQuestionsNumber() : questionnaireSize;
			questionsLoaded = true;
			if (questionBank.getChaptersNumber() == 0) {
				throw K::NO_CHAPTERS;
			}
			else if (this->questionnaireSize == 0) {
				throw K::NO_QUESTIONS;
			}
			QMetaObject::invokeMethod(
				this,
				[=]() {
					initQuestionList();
					initQuestionTable();
					ui.startButton->setEnabled(ui.lineEditName->text().length() > K::MINIMUM_NAME_SIZE);
				});
		}
		catch (const QString& errorMessage) {
			QMetaObject::invokeMethod(
				this,
				[=]() {
					showError(errorMessage);
				});
			logger->log(errorMessage.toStdString(), Logger::Level::ERROR);
		}
		}).detach();
}

UserInterface::~UserInterface() {
	loggerStream.close();
}


void UserInterface::init(const std::string& programName) {
	ui.setupUi(this);
	initWindow(QString::fromStdString(programName));
	initMainFrame();
	initQuestionnaireFrame();
	initQuestionListFrame();
	initResultFrame();
}

void UserInterface::initWindow(const QString& programName) {
	QSize size = QDesktopWidget().availableGeometry(this).size();
	setMinimumSize(size / 1.5);
	resize(size * 0.67);
	setWindowTitle(programName);
	setMainLabelText(programName);
}

void UserInterface::initMainFrame() {
	ui.lineEditName->installEventFilter(this);
	connect(ui.startButton, SIGNAL(clicked()), this, SLOT(startButtonClicked()));
	connect(ui.lineEditName, SIGNAL(textEdited(QString)), this, SLOT(textEntered(QString)));
}

void UserInterface::initQuestionnaireFrame() {
	ui.questionnaireFrame->setVisible(false);
	for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
		singleChoiceButtons[index] = ui.centralWidget->findChild<CustomRadioButton*>(K::SINGLE_CHOICE + QString::number(index + 1));
		multipleChoiceButtons[index] = ui.centralWidget->findChild<CustomCheckBox*>(K::MULTIPLE_CHOICE + QString::number(index + 1));
	}

	qTimer = new QTimer(this);
	if (showTimer) {
		connect(qTimer, SIGNAL(timeout()), this, SLOT(updateTimer()));
		qTimer->start();
	}
	else {
		ui.timerLabel->setVisible(false);
	}

	connect(ui.resetAnswer, SIGNAL(clicked()), this, SLOT(resetAnswerClicked()));
	connect(ui.flag, SIGNAL(clicked()), this, SLOT(flagClicked()));
	connect(ui.previousQuestion, SIGNAL(clicked()), this, SLOT(previousQuestionClicked()));
	connect(ui.nextQuestion, SIGNAL(clicked()), this, SLOT(nextQuestionClicked()));
	connect(ui.finishQuestionnaire, SIGNAL(clicked()), this, SLOT(finishQuestionnaireClicked()));
}

void UserInterface::initQuestionListFrame() {
	ui.questionListFrame->setVisible(false);
	connect(ui.showGrade, SIGNAL(clicked()), this, SLOT(showGradeClicked()));
}

void UserInterface::initResultFrame() {
	ui.resultFrame->setVisible(false);
	connect(ui.restart, SIGNAL(clicked()), this, SLOT(restartQuestionnaireClicked()));
}

void UserInterface::initQuestionList() {
	for (uint16_t index = 0; index < questionnaireSize; ++index) {
		QListWidgetItem* questionListItem = new QListWidgetItem;
		ui.questionListWidget->addItem(questionListItem);
	}
	connect(ui.questionListWidget, SIGNAL(itemClicked(QListWidgetItem*)), SLOT(listItemClicked(QListWidgetItem*)));
}

void UserInterface::initQuestionTable() {
	uint16_t rowsDivide = questionnaireSize / K::MAXIMUM_COLUMNS_QUESTION_TABLE;
	uint16_t rowsCount = questionnaireSize % K::MAXIMUM_COLUMNS_QUESTION_TABLE == 0
		? rowsDivide
		: rowsDivide + 1;
	uint8_t columnCount = rowsCount == 1 ? questionnaireSize : K::MAXIMUM_COLUMNS_QUESTION_TABLE;
	ui.questionTableWidget->setRowCount(rowsCount);
	ui.questionTableWidget->setColumnCount(columnCount);
	uint16_t questionIndex;
	for (uint16_t indexRow = 0; indexRow < rowsCount; indexRow++) {
		for (uint8_t indexColumn = 0; (questionIndex =
			indexRow * K::MAXIMUM_COLUMNS_QUESTION_TABLE + indexColumn + 1)
			<= questionnaireSize; ++indexColumn) {
			QTableWidgetItem* questionTableItem = new QTableWidgetItem;
			questionTableItem->setText(QString::number(questionIndex));
			questionTableItem->setTextAlignment(Qt::AlignCenter);
			ui.questionTableWidget->setItem(indexRow, indexColumn, questionTableItem);
		}
	}
	ui.questionTableWidget->resizeColumnsToContents();
	ui.questionTableWidget->resizeRowsToContents();
	ui.questionTableWidget->setMinimumWidth(ui.questionTableWidget->columnWidth(0) * K::MAXIMUM_COLUMNS_QUESTION_TABLE +
		ui.questionTableWidget->columnWidth(0) / 3);
	ui.questionTableWidget->horizontalHeader()->setVisible(false);
	ui.questionTableWidget->verticalHeader()->setVisible(false);
	connect(ui.questionTableWidget, SIGNAL(cellClicked(int, int)), SLOT(tableItemClicked(int, int)));
}

void UserInterface::showError(const QString& error) {
	setMainLabelText(error);
	ui.mainLabel->setStyleSheet("QLabel { color : red; font: 18pt; font-weight: bold;}");
	ui.bottomFrame->setVisible(false);
}

void UserInterface::setMainLabelText(const QString& message) {
	ui.mainLabel->setText(message);
}

void UserInterface::updateQuestionInfo() {
	std::shared_ptr<Question> currentQuestion = questionnaire.getQuestion(currentIndex);
	QString questionText = getFormattedQuestionText(currentIndex);
	std::string previouslySelectedAnswer = questionnaire.getAnswer(currentIndex);
	ui.flag->setText(questionnaire.getFlag(currentIndex) ? K::REMOVE_FLAG : K::ADD_FLAG);
	ui.questionLabel->setText(questionText);
	if (std::shared_ptr<SingleChoiceQuestion> singleChoiceQuestion = std::dynamic_pointer_cast<SingleChoiceQuestion>(currentQuestion)) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
			singleChoiceButtons[index]->setText(QString::fromStdString(singleChoiceQuestion->getQuestionAnswer(index)));
			singleChoiceButtons[index]->setAutoExclusive(false);
			singleChoiceButtons[index]->setChecked(false);
			singleChoiceButtons[index]->setAutoExclusive(true);
		}
		if (!previouslySelectedAnswer.empty()) {
			singleChoiceButtons[std::stoi(previouslySelectedAnswer)]->setChecked(true);
		}
		ui.stackedWidget->setCurrentIndex(K::SINGLE_CHOICE_INDEX_PAGE);
	}
	else if (std::shared_ptr<MultipleChoiceQuestion> multipleChoiceQuestion = std::dynamic_pointer_cast<MultipleChoiceQuestion>(currentQuestion)) {
		for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
			multipleChoiceButtons[index]->setText(QString::fromStdString(multipleChoiceQuestion->getQuestionAnswer(index)));
			multipleChoiceButtons[index]->setChecked(!previouslySelectedAnswer.empty() && previouslySelectedAnswer[index] == '1');
		}
		ui.stackedWidget->setCurrentIndex(K::MULTIPLE_CHOICE_INDEX_PAGE);
	}
	else if (std::dynamic_pointer_cast<InputQuestion>(currentQuestion)) {
		ui.inputQuestionAnswer->setText(QString::fromStdString(previouslySelectedAnswer));
		ui.stackedWidget->setCurrentIndex(K::INPUT_INDEX_PAGE);
	}
}

void UserInterface::updateQuestionLayout() {
	setCurrentTableItemSelected(true);
	if (currentIndex == 0) {
		ui.previousQuestion->setEnabled(false);
	}
	else {
		ui.previousQuestion->setEnabled(true);
	}
	if (currentIndex == questionnaireSize - 1) {
		ui.nextQuestion->setEnabled(false);
	}
	else {
		ui.nextQuestion->setEnabled(true);
	}
	updateQuestionInfo();
}

void UserInterface::saveAnswer() {
	std::string answer;
	switch (ui.stackedWidget->currentIndex()) {
	case K::SINGLE_CHOICE_INDEX_PAGE: {
		answer = getAnswerSingleChoice();
		break;
	}
	case K::MULTIPLE_CHOICE_INDEX_PAGE: {
		answer = getAnswerMultipleChoice();
		break;
	}
	case K::INPUT_INDEX_PAGE:
		answer = ui.inputQuestionAnswer->text().toStdString();
		break;
	}
	questionnaire.updateAnswer(currentIndex, answer);
	ui.questionTableWidget->item(currentIndex / K::MAXIMUM_COLUMNS_QUESTION_TABLE, currentIndex % K::MAXIMUM_COLUMNS_QUESTION_TABLE)
		->setBackgroundColor(answer.empty() ? K::BASE_TABLE_COLOR : K::ANSWERED_BACKGROUND_COLOR);
}

std::string UserInterface::getAnswerSingleChoice() const {
	uint8_t selectedAnswer = UINT8_MAX;
	for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
		if (singleChoiceButtons[index]->isChecked()) {
			selectedAnswer = index;
			break;
		}
	}
	return selectedAnswer == UINT8_MAX ? std::string() : std::to_string(selectedAnswer);
}

std::string UserInterface::getAnswerMultipleChoice() const {
	bool minimumOneAnswerChecked = false;
	std::stringstream stream;
	for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
		bool answerChecked = multipleChoiceButtons[index]->isChecked();
		stream << answerChecked;
		if (answerChecked) {
			minimumOneAnswerChecked = true;
		}
	}
	return minimumOneAnswerChecked ? stream.str() : std::string();
}

void UserInterface::saveResult(float grade) {
	auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	struct tm time;
	localtime_s(&time, &now);
	std::ostringstream ost;
	ost << std::put_time(&time, "_%H-%M-%S_%d-%m-%Y");
	std::ofstream result("../output/" + user.getName() + ost.str() + ".txt");
	result << K::USER_NAME << user << std::endl;
	result << K::USER_GRADE << grade << std::endl;
	std::time_t timeStart = std::chrono::system_clock::to_time_t(timer.getStart());
	result << K::START_QUESTIONNAIRE_TIME << std::ctime(&timeStart);
	std::time_t timeEnd = std::chrono::system_clock::to_time_t(timer.getEnd());
	result << K::FINISH_QUESTIONNAIRE_TIME << std::ctime(&timeEnd);
	result << std::endl << K::USER_ANSWERS << std::endl << std::endl;
	for (uint16_t index = 0; index < questionnaireSize; ++index) {
		std::shared_ptr<Question> currentQuestion = questionnaire.getQuestion(index);

		result << getFormattedQuestionText(index).toStdString() << (questionnaire.getFlag(index) ? " - FLAGGED" : std::string()) << std::endl;
		if (questionnaire.getAnswer(index).empty()) {
			result << std::endl;
			continue;
		}
		if (std::shared_ptr<SingleChoiceQuestion> singleChoiceQuestion = std::dynamic_pointer_cast<SingleChoiceQuestion>(currentQuestion)) {
			result << singleChoiceQuestion->getQuestionAnswer(std::stoi(questionnaire.getAnswer(index))) << std::endl;
		}
		else if (std::shared_ptr<MultipleChoiceQuestion> multipleChoiceQuestion = std::dynamic_pointer_cast<MultipleChoiceQuestion>(currentQuestion)) {
			std::string answer = questionnaire.getAnswer(index);
			for (uint8_t index = 0; index < ChoiceQuestion::ANSWERS_NUMBER; ++index) {
				if (answer[index] - '0' == 1) {
					result << multipleChoiceQuestion->getQuestionAnswer(index) << std::endl;
				}
			}
		}
		else if (std::shared_ptr<InputQuestion> inputQuestion = std::dynamic_pointer_cast<InputQuestion>(currentQuestion)) {
			result << questionnaire.getAnswer(index) << std::endl;
		}
		result << std::endl;
	}
}

QString UserInterface::getFormattedQuestionText(uint16_t index) {
	return QString::number(index + 1) + ". " + QString().fromStdString(questionnaire.getQuestion(index)->getQuestionText());
}

void UserInterface::setCurrentTableItemSelected(bool selected) {
	if (currentIndex < questionnaireSize) {
		ui.questionTableWidget->item(currentIndex / K::MAXIMUM_COLUMNS_QUESTION_TABLE,
			currentIndex % K::MAXIMUM_COLUMNS_QUESTION_TABLE)->setSelected(selected);
	}
}

void UserInterface::buttonClicked(const QString& buttonName) {
	logger->log(buttonName.toStdString() + K::LOG::PRESSED, Logger::Level::INFO);
}

void UserInterface::previousNextQuestionClicked(bool previous) {
	saveAnswer();
	setCurrentTableItemSelected(false);
	if (previous) {
		--currentIndex;
	}
	else {
		++currentIndex;
	}
	updateQuestionLayout();
}

bool UserInterface::eventFilter(QObject* obj, QEvent* event) {
	if (event->type() == QEvent::KeyPress) {
		QKeyEvent* key = static_cast<QKeyEvent*>(event);
		if ((key->key() == Qt::Key_Enter) || (key->key() == Qt::Key_Return)
			&& ui.lineEditName->text().length() > K::MINIMUM_NAME_SIZE && questionsLoaded) {
			QApplication::focusWidget()->clearFocus();
			startButtonClicked();
			return true;
		}
	}
	return QObject::eventFilter(obj, event);
}

void UserInterface::startButtonClicked() {
	user = User(ui.lineEditName->text().toStdString());
	questionnaire = questionBank.createQuestionnaire(questionnaireSize);
	timer.Start();
	if (showTimer) {
		qTimer->start();
	}
	currentIndex = 0;
	updateQuestionLayout();
	ui.mainFrame->setVisible(false);
	ui.questionnaireFrame->setVisible(true);
	buttonClicked(K::LOG::START_BUTTON);
}

void UserInterface::previousQuestionClicked() {
	previousNextQuestionClicked(true);
	buttonClicked(K::LOG::PREVIOUS_QUESTION);
}

void UserInterface::nextQuestionClicked() {
	previousNextQuestionClicked(false);
	buttonClicked(K::LOG::NEXT_QUESTION);
}

void UserInterface::finishQuestionnaireClicked() {
	saveAnswer();
	for (uint16_t index = 0; index < questionnaireSize; ++index) {
		QString formattedQuestion = getFormattedQuestionText(index);
		formattedQuestion = formattedQuestion.size() > K::MAXIMUM_QUESTION_LENGTH ?
			formattedQuestion.left(K::MAXIMUM_QUESTION_LENGTH) + "..."
			: formattedQuestion;
		QColor backgroundColor = K::ANSWER_BACKGROUND_COLOR;
		QColor textColor = questionnaire.getFlag(index) ? K::FLAG_COLOR : K::BASE_COLOR;
		if (questionnaire.getAnswer(index).empty()) {
			formattedQuestion += K::NO_ANSWER;
			backgroundColor = K::NO_ANSWER_BACKGROUND_COLOR;
		}
		ui.questionListWidget->item(index)->setBackgroundColor(backgroundColor);
		ui.questionListWidget->item(index)->setText(formattedQuestion);
		ui.questionListWidget->item(index)->setTextColor(textColor);
	}

	ui.questionnaireFrame->setVisible(false);
	ui.questionListFrame->setVisible(true);
	setCurrentTableItemSelected(false);
	buttonClicked(K::LOG::FINISH_QUESTIONNAIRE);
}

void UserInterface::restartQuestionnaireClicked() {
	ui.resultFrame->setVisible(false);
	ui.mainFrame->setVisible(true);
	buttonClicked(K::LOG::RESTART_QUESTIONNAIRE);
	for (uint16_t index = 0; index < questionnaireSize; ++index) {
		ui.questionTableWidget->item(index / K::MAXIMUM_COLUMNS_QUESTION_TABLE,
			index % K::MAXIMUM_COLUMNS_QUESTION_TABLE)->setBackgroundColor(K::BASE_TABLE_COLOR);
		ui.questionTableWidget->item(index / K::MAXIMUM_COLUMNS_QUESTION_TABLE,
			index % K::MAXIMUM_COLUMNS_QUESTION_TABLE)->setTextColor(K::BASE_COLOR);
	}
}

void UserInterface::showGradeClicked() {
	timer.Stop();
	if (showTimer) {
		qTimer->stop();
	}
	float grade = questionnaire.calculateGrade();
	ui.resultLabel->setText(K::RESULT_TEXT + QString::number(grade));
	ui.questionListFrame->setVisible(false);
	ui.resultFrame->setVisible(true);
	saveResult(grade);
	buttonClicked(K::LOG::SHOW_GRADE);
}

void UserInterface::textEntered(const QString& text) {
	ui.startButton->setEnabled(text.length() > K::MINIMUM_NAME_SIZE && questionsLoaded);
}

void UserInterface::listItemClicked(QListWidgetItem* item) {
	currentIndex = ui.questionListWidget->currentIndex().row();
	updateQuestionLayout();
	ui.questionListFrame->setVisible(false);
	ui.questionnaireFrame->setVisible(true);
	item->setSelected(false);
	buttonClicked(K::LOG::LIST_ITEM + getFormattedQuestionText(currentIndex));
}

void UserInterface::tableItemClicked(int row, int column) {
	if (row * K::MAXIMUM_COLUMNS_QUESTION_TABLE + column < questionnaireSize) {
		saveAnswer();
		currentIndex = row * K::MAXIMUM_COLUMNS_QUESTION_TABLE + column;
		updateQuestionLayout();
		buttonClicked(K::LOG::TABLE_ITEM + getFormattedQuestionText(currentIndex));
	}
}

void UserInterface::updateTimer() {
	uint32_t secondsElapsed = timer.getTimeDelta();
	uint32_t minute = minutes - (secondsElapsed / 60) - 1;
	uint16_t second = 59 - (secondsElapsed % 60);
	QString minuteString = QString(minute < 10 ? "0" + QString::number(minute) : QString::number(minute));
	QString secondString = QString(second < 10 ? "0" + QString::number(second) : QString::number(second));
	QString timerText = minuteString + ":" + secondString;
	ui.timerLabel->setText(timerText);
	ui.timerLabelQuestionList->setText(timerText);

	if (minute == 0 && second == 0) {
		qTimer->stop();
		timer.Stop();
		ui.questionnaireFrame->setVisible(false);
		showGradeClicked();
	}
}

void UserInterface::resetAnswerClicked() {
	questionnaire.updateAnswer(currentIndex, std::string());
	buttonClicked(K::LOG::RESET_ANSWER);
	updateQuestionInfo();
}

void UserInterface::flagClicked() {
	questionnaire.updateFlag(currentIndex, !questionnaire.getFlag(currentIndex));
	if (questionnaire.getFlag(currentIndex)) {
		buttonClicked(K::LOG::ADD_FLAG_QUESTION + getFormattedQuestionText(currentIndex));
		ui.flag->setText(K::REMOVE_FLAG);
		ui.questionTableWidget->item(currentIndex / K::MAXIMUM_COLUMNS_QUESTION_TABLE,
			currentIndex % K::MAXIMUM_COLUMNS_QUESTION_TABLE)->setTextColor(K::FLAG_COLOR);
	}
	else {
		buttonClicked(K::LOG::REMOVE_FLAG_QUESTION + getFormattedQuestionText(currentIndex));
		ui.flag->setText(K::ADD_FLAG);
		ui.questionTableWidget->item(currentIndex / K::MAXIMUM_COLUMNS_QUESTION_TABLE,
			currentIndex % K::MAXIMUM_COLUMNS_QUESTION_TABLE)->setTextColor(K::BASE_COLOR);
	}
}