#include "User.h"

User::User(const std::string& name)
	:name(name) {}

User::User(const User& other)
	: name(other.name) {}

User::User(User&& other) {
	*this = std::forward<User&&>(other);
}

User& User::operator=(const User& other) {
	name = other.name;
	return *this;
}

User& User::operator=(User&& other) {
	name = std::move(other.name);
	return *this;
}

const std::string& User::getName() const noexcept {
	return name;
}

std::ostream& operator<<(std::ostream& output, const User& user) {
	output << user.name;
	return output;
}
