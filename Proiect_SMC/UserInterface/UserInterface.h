#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <QtWidgets/QMainWindow>
#include <QStackedWidget>
#include <QCheckBox>
#include <QListWidget>
#include <QTableWidget>
#include <QTimer>

#include "ui_UserInterface.h"

#include "../Logger/Logger.h"
#include "../QuestionnaireFramework/ChoiceQuestion.h"
#include "../QuestionnaireFramework/QuestionBank.h"

#include "UserConstants.h"
#include "CustomRadioButton.h"
#include "CustomCheckBox.h"
#include "User.h"
#include "Timer.h"

class UserInterface : public QMainWindow {
private:
	Q_OBJECT;
	Ui::ProgramClass ui;

	std::ofstream loggerStream;
	std::shared_ptr<Logger> logger;

	User user;
	QuestionBank questionBank;
	Questionnaire questionnaire;
	uint16_t questionnaireSize;
	std::atomic_bool questionsLoaded = false;
	uint16_t currentIndex = 0;

	std::array<CustomRadioButton*, ChoiceQuestion::ANSWERS_NUMBER> singleChoiceButtons;
	std::array<CustomCheckBox*, ChoiceQuestion::ANSWERS_NUMBER> multipleChoiceButtons;

	QTimer* qTimer;
	bool showTimer;
	Timer timer;
	uint16_t minutes;

public:
	UserInterface(const std::string& questionsFile,
		const std::string& programName = K::PROGRAM_NAME,
		uint16_t questionnaireSize = K::QUESTIONNAIRE_SIZE,
		bool showTimer = true,
		uint16_t minutes = K::DEFAULT_TIMER_MINUTES,
		QWidget* parent = Q_NULLPTR);
	~UserInterface();

private:
	void init(const std::string& programName = K::PROGRAM_NAME);
	void initWindow(const QString& programName);
	void initMainFrame();
	void initQuestionnaireFrame();
	void initQuestionListFrame();
	void initResultFrame();
	void initQuestionList();
	void initQuestionTable();
	void showError(const QString& error);
	void setMainLabelText(const QString& message);
	void updateQuestionInfo();
	void updateQuestionLayout();
	void saveAnswer();
	std::string getAnswerSingleChoice() const;
	std::string getAnswerMultipleChoice() const;
	void saveResult(float grade);
	void buttonClicked(const QString& buttonName);
	QString getFormattedQuestionText(uint16_t index);
	void setCurrentTableItemSelected(bool selected);
	void previousNextQuestionClicked(bool previous);
	bool eventFilter(QObject* obj, QEvent* event);

private slots:
	void startButtonClicked();
	void previousQuestionClicked();
	void nextQuestionClicked();
	void finishQuestionnaireClicked();
	void restartQuestionnaireClicked();
	void showGradeClicked();
	void textEntered(const QString& text);
	void listItemClicked(QListWidgetItem* item);
	void tableItemClicked(int row, int column);
	void updateTimer();
	void resetAnswerClicked();
	void flagClicked();
};