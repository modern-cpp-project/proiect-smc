#pragma once

#include <chrono>

class Timer{
private:
	std::chrono::time_point<std::chrono::system_clock> start;
	std::chrono::time_point<std::chrono::system_clock> end;
	bool running;

public:
	Timer();
	const std::chrono::time_point<std::chrono::system_clock>& getStart() const;
	const std::chrono::time_point<std::chrono::system_clock>& getEnd() const;
	void Start();
	void Stop();
	const uint32_t getTimeDelta() const;
};