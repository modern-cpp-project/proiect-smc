#include <QtWidgets/QApplication>

#include "UserInterface.h"

int main(int argc, char* argv[]) {
	QApplication app(argc, argv);
	UserInterface userInterface("../input/input.txt");
	userInterface.show();
	return app.exec();
}