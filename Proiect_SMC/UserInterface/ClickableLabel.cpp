#include "ClickableLabel.h"

ClickableLabel::ClickableLabel(QWidget* parent)
	:QLabel(parent) {}

ClickableLabel::~ClickableLabel() = default;

void ClickableLabel::mousePressEvent(QMouseEvent * event) {
	emit clicked();
}
