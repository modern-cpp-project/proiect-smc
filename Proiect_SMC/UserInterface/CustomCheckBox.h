#pragma once

#include <QCheckBox>
#include <QHBoxLayout>

#include "WordWrapWidget.h"

class CustomCheckBox : public QCheckBox {
private:
	Q_OBJECT;
	std::unique_ptr<QHBoxLayout> layout;
	std::unique_ptr<ClickableLabel> label;
	std::unique_ptr<WordWrapWidget> wordWrapWidget;

public:
	CustomCheckBox(QWidget* parent = Q_NULLPTR);
	void setText(const QString& text);
	QSize sizeHint() const override;

private slots:
	void labelIsClicked();

protected:
	void resizeEvent(QResizeEvent* event) override;

private:
	void init();
};
