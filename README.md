## Questionnaire Framework

Questionnaire Framework represents the basis of a modern tool for verifying knowledge in a particular field, in a concise and correct way, more precisely a digital questionnaire.

[Questionnaire Framework - Video Demo](https://youtu.be/DYl23tiZ_Ng)

Project made by:

* Stanciu Marius-Gabriel
* Pascale George-Andrei
* Pamfile Alex
* Necula Bogdan Alexandru